# account-tender

The account-tender is a service for handling user account information.

## Licensing
This module is unlicensed public domain open source.
See the UNLICENSE file or unlicense.org for more information.

## Installation and configuration
Get the module and other required modules with npm install,
then set database access and service port data in config.js file.
Create database role, database, and database tables
with create-db-role.sh, create-db.sh, and create-db-tables.sh.

## Quick use
Start and test with npm start and npm test.

## General use
Start account-tender with npm start and connect using port from configuration.

## Documentation
The docs folder has
a manual which describes basic usage
a guide for developing that describes some internals
and release notes with details about specific releases.
Documentation is in HTML format for readability and editability.

