// permit.js
// basic generation, publishing, and cancellation of permits

// log with pino
let pino = require('pino')();
// uuid generation for new records
let { v4: uuidv4 } = require('uuid');

// permitCreate is used to issue new permits when authorized
// accountRef is primary used reference
// accountName is retained for convenience
// host and agent are stored to detect bad moves and users
// role and status come from the account getting the permit
// and are used to determine authorization
// callback gets summary of results
function permitCreate (pgClient, accountRef, host, agent, role, status, callback) {
    let newPermitRef = uuidv4();
    let newPermitQuery ='INSERT INTO permit'
        + ' (ref, account, host, agent, role, status) VALUES (\''
        + newPermitRef + '\', \''
        + accountRef + '\', \''
        + host + '\', \''
        + agent + '\', \''
        + role + '\', \''
        + status + '\');';
    pgClient.query(newPermitQuery, function(err) {
        if (err) {
            pino.error('error posting new permit: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        // need to mark step as finished or delete
        callback({result: 'okay', reason: 'response-valid', ref: newPermitRef
        });
        return;
    });
}

// permitVerify checks a permit for an action
// pgClient is the database
// account and permit are actor and credentials respectively
// callback gets results
function permitVerify (pgClient, accountRef, permitRef, usage, callback) {
    // check account
    // uncredentialed accounts need no permits
    // status banned accounts fail even with permits
    // permits must be current, valid, and match account
    let accountQuery = 'SELECT credentialed,roles,status FROM account ' + 'WHERE ref=\'' + accountRef + '\';';
    pino.info('account query string: %s', accountQuery);
    pgClient.query(accountQuery, function(err, accountResult) {
        if (err) {
            pino.error('error reading account: %s', err);
            callback({result: 'failed', reason: 'account-invalid'});
            return;
        }
        if (accountResult.rowCount === 0) {
            pino.error('account not found: %s', accountRef);
            callback({result: 'failed', reason: 'account-invalid'});
            return;
        }
        if ((accountResult.rows[0].credentialed === undefined) 
        || (accountResult.rows[0].credentialed === null)) {
            callback({result: 'okay', reason: 'account-incomplete'});
            return;
        }
        pino.info('permit check account query count: %d, rows: %s', accountResult.rowCount, JSON.stringify(accountResult.rows));
        // check permit 
        let permitQuery = 'SELECT account,issued,host,agent,role,status'
            + ' FROM permit WHERE ref=\'' + permitRef + '\';';
        pino.info('permit check query string: %s', permitQuery);
        pgClient.query(permitQuery, function(err, permitResult) {
            if (err) {
                pino.error('error looking up permit: %s', err);
                callback({result: 'failed', reason: 'permit-invalid'});
                return;
            }
            if (permitResult.rowCount === 0) {
                // no such permit found, inherently invalid, no authority
                pino.error('permit not found: %s', permitRef);
                callback({result: 'failed', reason: 'permit-invalid'});
                return;
            }
            // check account status, if not okay then fail ...
            if (permitResult.rows[0].account != accountRef) {
                // permit account name mismatch
                pino.error('request and permit account mismatch: %s not %s', accountRef, permitResult.rows[0].account);
                callback({result: 'failed', reason: 'permit-invalid'});
                return;
            }
            // !!! usage: ownership means permit and actor account must match
            //     and role means account roles must include that role
            callback({result: 'okay', reason: 'permit-read', 
                account: accountResult.rows[0].ref});
            return;
        });  // end permit query block
    }); // end account check block
}

// permitCancel invalidates a permit
// pgClient is the database
// permit is a direct reference uuid
// calllback gets the result
function permitCancel (pgClient, permit, callback) {
    // check permit first, then account next if okay
    let permitQuery = 'SELECT ref,account,issued,role,status FROM permit '
        + 'WHERE ref=\'' + permit + '\';';
    pgClient.query(permitQuery, function (err, result) {
        if (err) {
            pino.error('error checking permit: %s', err);
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // check permit is current, valid, matches account
        if (result.rowCount === 0) {
            pino.error('permit not found, invalid');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        if (result.rows[0].status != 'okay') {
            pino.error('permit status not okay: %s', result.rows[0].status);
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // no account check required, any permit can be cancelled
        let deletionQuery = 'DELETE FROM permit WHERE ref=\'' + permit + '\';';
        pgClient.query(deletionQuery, function(err) {
            if (err) {
                pino.error('error deleting permit: %s', err);
                callback({result: 'failed', reason: 'failed'});
                return;
            }
            callback({result: 'done', reason: 'permit-cancelled'});
            return;
        });
    });  // end permit check block
}

// object to be exported by module
var permit = {};
permit.create = permitCreate;
permit.check = permitVerify;
permit.cancel = permitCancel;
module.exports = permit;
