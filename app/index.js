'use strict';
// index module loads resources and routes calls to specific modules

// internal modules for configuration and abstractions
let config = require('./config');
let property = require('./property');
let account = require('./account');
let permit = require('./permit');
//let persona = require('./persona');
//let attribute = require('./attribute');  // used for direct attribute save and delete

// external modules for the rest
// logging with pino
let pino = require('pino')();
// email routines unready: let nodemailer = require('nodemailer');
// express and related middleware
let express = require('express');
let app = express();
let http = require('http').Server(app);
let bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
// postgres adaptor
let pg = require('pg');
let pgClient = new pg.Client(config.dbAccessString);
pgClient.connect(function (err) {
    if (err) {
        pino.error('account tender could not connect to postgres: %s', err);
    }
});

// TENTATIVE CHANGES TO URIS
// 
// /property/:name - service parameters
// /request - request a new account
// /activate - activate account with invitation
// /secure - set account password with permit from invitation
// /invite - generate invitation for account
// /inspect - recall account data
// /update - modify account data
// /sign - authenticate account access
// /permit - check authorization for action based on permit
// /revoke - cancel authorization permit

// property routing only for fetching operational parameters
// fetch service properties including service header
app.get('/property/:name', function getServiceProperty(req, res) {
    pino.info('GET /property with name = %s', req.params.name);
    property.get(pgClient, req.params.name, function (result) {
        pino.info('result of property fetch: %s', JSON.stringify(result));
        res.send(result);
    });
});

// account actions: request, references, challenge, response, inspect, update, reset, delete

// activate account invitation
app.post('/activate', function postActivation(req, res) {
    pino.info('POST /activate, code = %s', req.body.code);
    account.activate(pgClient, req.body.code, function(result) {
        pino.info('account invitation activation');
        res.send(result);
        return;
    });
});

// secure account
app.post('/secure', function postSecure(req, res) {
    pino.info('POST /secure, account = %s, permit = %s', req.body.account, req.body.permit);
    account.secure(pgClient, req.body.account, req.body.permit, function(result) {
        pino.info('account secure result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

// posting an account request does not necessarily generate an account or response
// it may be necessary to poll for existence of the account later
// though initial implementation should always create an account and return references
app.post('/request', function postAccountProperty(req, res) {
    pino.info('POST /account/request, body: %s', JSON.stringify(req.body));
    // sanity checks of required inputs
    // assemble account request from given parameters
    let accountRequest = {
        credentials: req.body.credentials,
        attributes: req.body.attributes
    };
    pino.info('initiating account request: %s', JSON.stringify(accountRequest));
    account.request(pgClient, accountRequest, function (result) {
        pino.info('account request result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

// posting account credentials enables sign in
// OUTDATED
//app.post('/account/credential', function postAccountCredential(req, res) {
//    pino.info('POST /account/credential, args: %s', JSON.stringify(req.body));
//    if (req.body.account === undefined || req.body.account === null || req.body.account === '') {
//        pino.info('post account credential without valid account reference');
//        res.send({result: 'failed', reason: 'account-invalid'});
//        return;
//    }
//    account.credential(pgClient, req.body.account, req.body.challenge, req.body.response, req.body.confirm, function(result) {
//        pino.info('account credential result: %s', JSON.stringify(result));
//        res.send(result);
//    });
//});

// completing an account sets credentials in place
// and fully enables sign in to the account
// OUTDATED
//app.post('/account/complete/:ref', function postAccountComplete(req, res) {
//    pino.info('POST /account/complete, ref: %s', JSON.stringify(req.params.ref));
//    account.complete(pgClient, req.params.ref, function(result) {
//        pino.info('account complete result: %s', JSON.stringify(result));
//        res.send(result);
//    });
//});

// tests if referenced account has complete credentials for signing in
// OUTDATED
//app.get('/account/credentialed/:ref', function getAccountCredentialed(req, res) {
//    pino.info('GET /account/credentialed, ref = %s', req.params.ref);
//    account.credentialed(pgClient, req.params.ref, function(result) {
//        pino.info('account credentialed result: %s', JSON.stringify(result));
//        res.send(result);
//    });
//});

// get account references
// works by taking attributes
// and returning a direct reference
// to the only acount having those attribute values
// OUTDATED
//app.get('/account/references', function getAccountReferences(req, res) {
//    pino.info('GET /account/references, name = %s, contact = %s, address = %s, pronoun = %s', req.body.name, req.body.contact, req.body.address, req.body.pronoun);
//    account.references(pgClient, req.body.name, req.body.contact, req.body.address, req.body.pronoun, function(result) {
//        pino.info('account references request result: %s', JSON.stringify(result));
//        res.send(result);
//        return
//    });
//});

// getting an account challenge initiates the sign in process
app.get('/account/challenge', function getAccountProperty(req, res) {
    pino.info('GET /account/challenge, body = %s', JSON.stringify(req.body));
    if (req.body.account === undefined || req.body.account === '') {
        // no direct reference, so derive that from other parameters
        account.references(pgClient, req.body.name, req.body.contact, req.body.address, req.body.pronoun, function(refResult) {
            // now get challenge using evaluated direct account reference
            pino.info('got reference = %s',
                req.body.name, refResult.account);
            account.challenge(pgClient, refResult.account,
                req.headers.host, req.headers['user-agent'], function (result) {
                pino.info('account challege result: %s', JSON.stringify(result));
                res.send(result);
                return;
            });       
        });
    }
    else {
        // direct reference present, so use that without any evaluations
        account.challenge(pgClient, req.body.account,
            req.headers.host, req.headers['user-agent'], function (result) {
            pino.info('account challege result: %s', JSON.stringify(result));
            res.send(result);
            return;
        });       
    }
});

// post responses to account challenges in order to sign in
app.post('/account/response', function postAccountResponse(req, res) {
    // return okay, signed, invalid, failed
    pino.info('POST /account/response with account = %s, response = %s', req.body.account, req.body.response);
    // check account, and response are nonempty
    // should actually be name or full name or direct reference being nonempty
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('failed, account reference required');
        res.send({result: 'failed', reason: 'account-reference-required'});
        return;
    }
    if (req.body.response === undefined || req.body.response === '') {
        pino.error('failed, response required');
        res.send({result: 'failed', reason: 'response-required'});
        return;
    }
    // name, full name, or reference and response are the only relevent fields?
    account.response(pgClient, req.body.account, req.body.response, 
        req.headers.host, req.headers['user-agent'], function (result) {
        pino.info('account response result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
}); 

// fetch account records
// returns account and attribute data
// requires account and permit for permission
// uses "ref" for direct account references
// and requires either ref or unambiguous name, contact, address data
app.get('/inspect', function getAccountInspect(req, res) {
    pino.info('GET /account/inspect');
    // return all basic account data, similar to get /account/:property
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('no account reference, returning failure');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    account.inspect(pgClient, req.body.account, req.body.permit, req.body.persona, function (result) {
        pino.info('account inspection result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});  // end inspection fetch request block

// post changes to account records
app.post('/update', function postAccountUpdate(req, res) {
    pino.info('POST /account/update, body = %s', JSON.stringify(req.body));
    // post updated account information
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('no account reference, returning failure');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('failed, account update requires permit');
        res.send({ result: 'failed', reason: 'permit-required' });
        return;
    }
    let accountRecord = {
        account: req.body.account,
        permit: req.body.permit,
        roles: req.body.roles,
        status: req.body.status
    };
    account.update(pgClient, accountRecord, req.body.attributes, function (result) {
        pino.info('account update result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});  // end account update route block

// generate a short string summarizing account data
// requires a reference but does not check permits as such
// because it is intended to be used
// by clients and providers together
// OUTDATED
//app.get('/account/summary', function getAccountSummary(req, res) {
//    pino.info('get account summary, account ref = %s', req.body.ref);
//    account.summary(req.body.ref, function(result) {
//        res.send({result: 'okay', reason: 'account-read', summary: result});
//    });
//});

// initiate account authentication credential reset process
// !!! INCOMPLETE FEATURE
// OUTDATED
//app.post('/account/reset', function postAccountReset(req, res) {
//    pino.info('POST /account/reset, body = %s', JSON.stringify(req.body.code));
    // required parameters must be defined and nonempty
//    if (req.body.name === undefined || req.body.name === '') {
//        pino.error('account name undefined or empty, failing');
//        res.send({result: 'failed', reason: 'account-name-required'});
//        return;
//    }
    // not clear what fields are neccessary, so send all
    // expected ref/name should be plenty to trigger action
//    let accountRecord = {
//        ref: req.body.ref,
//        permit: req.body.permit,
//        profile: req.body.profile,
//        permits: req.body.permits,
//        status: req.body.status
//    };
//    account.reset(pgClient, accountRecord, req.body.code,
//        req.headers.host, req.headers['user-agent'], function (result) {
//        pino.info('account reset result: %s', JSON.stringify(result));
//        res.send(result);
//        return;
//    });
//});  // end account reset block

// initiate or confirm account deletion process
app.post('/account/delete', function postAccountDelete(req, res) {
    pino.info('POST /account/delete, body = %s', JSON.stringify(req.body));
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('failed, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('failed, permit required');
        res.send({result: 'failed', reason: 'permit-required'});
        return;
    }
    account.delete(pgClient, req.body.account, req.body.permit,
        req.headers.host, req.headers['user-agent'], function (result) {
        pino.info('delete account initiation result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
}); 

// account attribute actions: save (new or updated data), delete
// at this time attributes used for account reference search and personas
// thus no external interfaces for fetching attributes are available
// instead they are included with account inspection
// also the attribute match search/test function is also only used inderectly
// note also most interfaces pass the permit along for contextual usage
// but in this case the attribute interfaces are internal and context does not vary
// so for atttibutes the permit is checked here at the routing step unlike others
// !!! ALL OUTDATED

// attribute save
// requires valid account and permit, checked with hardcoded usage
// expects value parameter with array of text and qualifier pairs
app.post('/attribute/save', function postAttributeSave(req, res) {
    pino.info('POST /attribute/save, body = %s', JSON.stringify(req.body));
    // save attribute to an uncredentialed account needs no permit ...
    // !!! PERMIT CHECKS SUSPENDED FOR CODE TESTING
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('failed, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    let postValues = [req.body.type, req.body.value, req.body.qualifier];
    attribute.post(pgClient, req.body.account, postValues, function(result) {
        pino.info('attribute post result', JSON.stringify(result));
        res.send(result);
        return;
    });
    if (false) { 
        permit.check(pgClient, req.body.account, req.body.permit, 'write', function(checkResult) {
            if (checkResult.result != 'okay') {
                pino.info('invalid permit, attribute save failed');
                callback({result: 'failed', reason: 'permit-invalid'});
                return;
            }
            if (req.body.old_text != undefined && req.body.old_text != '' && req.body.old_qualifier != undefined && req.body.old_qualifier != '') {
                // delete old values before adding new ones
                // note this is special to this case because text or qualifier ...
                attribute.delete(pgClient, req.body.account, req.body.attribute, req.body.old_text, req.body.old_qualifier, function(clearResult) {
                    pino.info('attribute post old values deletion result', JSON.stringify(clearResult));
                    let postValues = [req.body.new_text, req.body.new_qualifier];
                    attribute.post(pgClient, req.body.account, req.body.attribute, postValues, function(result) {
                        pino.info('attribute post result', JSON.stringify(result));
                        res.send(result);
                        return;
                    });
                });  // end attribute delete block
            }  // end old text and qualifier block
            else {
                let postValues = [req.body.new_text, req.body.new_qualifier];
                attribute.post(pgClient, req.body.account, req.body.attribute, postValues, function(result) {
                    pino.info('attribute post result', JSON.stringify(result));
                    res.send(result);
                    return;
                });
            }
        });  // end permit check block
    }  // if false--old code blocked from execution for experiments
});

// attribute delete
// requires account and permit
// parameters currently unclear, may need to call match to find first? 
app.post('/attribute/delete', function postAttributeDelete(req, res) {
    pino.info('POST /attribute/delete, body = %s', JSON.stringify(req.body));
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('failed, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    attribute.delete(pgClient, req.body.account, req.body.type, req.body.value, req.body.qualifier, function(result) {
        pino.info('attribute delete result', JSON.stringify(result));
        res.send(result);
        return;
    });
});

// persona actions: request, inspect, update
// !!! ALL PERSONA STUFF OUTDATED

// request persona
app.post('/persona/request', function personaRequest(req, res) {
    pino.info('POST /persona/request');
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error, permit required');
        res.send({ result: 'failed', reason: 'permit-required' });
        return;
    }
    persona.request(pgClient, req.body.account, req.body.permit, req.body.name, function (result) {
        pino.info('persona request result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

// inspect persona
app.get('/persona/inspect', function personaInspect(req, res) {
    pino.info('GET /persona/inspect');
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error, permit required');
        res.send({ result: 'failed', reason: 'permit-required' });
        return;
    }
    if (req.body.persona === undefined || req.body.persona === '') {
        pino.error('error, persona required');
        res.send({ result: 'failed', reason: 'persona-required' });
        return;
    }
    persona.inspect(pgClient, req.body.account, req.body.permit, req.body.persona, function(result) {
        pino.info('persona inspect result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

// fetch persona records for an account
app.get('/persona/records', function personaInspect(req, res) {
    pino.info('GET /persona/records');
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error, permit required');
        res.send({ result: 'failed', reason: 'permit-required' });
        return;
    }
    persona.records(pgClient, req.body.account, req.body.permit, function(result) {
        pino.info('persona records result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

app.post('/persona/update', function personaUpdate(req, res) {
    pino.info('POST /persona/update');
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error, permit required');
        res.send({ result: 'failed', reason: 'permit-required' });
        return;
    }
    let personaRecord = {
        qualifier: req.body.qualifier,
        name: req.body.name,
        contact: req.body.contact,
        address: req.body.address,
        pronoun: req.body.pronoun,
        notes: req.body.notes
    };
    persona.update(pgClient, req.body.account, req.body.permit, personaRecord, function(result) {
        pino.info('persona update result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

app.get('/persona/list', function personaList(req, res) {
    pino.info('GET /persona/list');
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error, permit required');
        res.send({ result: 'failed', reason: 'permit-required' });
        return;
    }
    persona.list(pgClient, req.body.account, req.body.permit, function(result) {
        pino.info('persona list result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

// persona toggle !!! parameters in flux
app.post('/persona/toggle', function personaToggle(req, res) {
    pino.info('POST /persona/toggle: persona = %s, attr = %s, value = %s, qual = %s, state = %s', req.body.persona, req.body.attribute, req.body.value, req.body.qualifier, JSON.stringify(req.body.state));
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error, permit required');
        res.send({ result: 'failed', reason: 'permit-required' });
        return;
    }
    // check other parameters? persona, attribute, value, qualifier, state
    persona.toggle(pgClient, req.body.account, req.body.permit, req.body.persona, req.body.attribute, req.body.value, req.body.qualifier, req.body.state, function(result) {
        pino.info('persona toggle result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

app.post('/persona/delete', function personaDelete(req, res) {
    pino.info('POST /persona/delete, name = %s', req.body.name);
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error, permit required');
        res.send({ result: 'failed', reason: 'permit-required' });
        return;
    }
    if (req.body.name === undefined || req.body.name === '') {
        pino.error('error, name required');
        res.send({ result: 'failed', reason: 'name-required' });
        return;
    }
    persona.delete(pgClient, req.body.account, req.body.permit, req.body.name, function(result) {
        pino.info('persona delete result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

// permit actions: validity check, cancellation

// check permit validity for account action
app.get('/permit', function permitValid(req, res) {
    pino.info('GET /permit, body = %s', JSON.stringify(req.body));
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error, account required');
        res.send({ result: 'failed', reason: 'account-required' });
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error, permit required');
        res.send({ result: 'failed', reason: 'permit-required' });
        return;
    }
    permit.check(pgClient, req.body.account, req.body.permit, req.body.usage, function (result) {
        pino.info('permit check result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});  

// cancel authentication permit
app.post('/revoke', function permitCancel(req, res) {
    pino.info('POST /revoke');
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('permit cancel requires permit');
        res.send({result: 'failed', reason: 'permit-required'});
        return;
    }
    permit.cancel(pgClient, req.body.permit, function (result) {
        pino.info('permit cancel result: %s', JSON.stringify(result));
        res.send(result);
        return;
    });
});

// start story account service
http.listen(config.servicePort, function() {
    pino.info('listening to port %d', config.servicePort);
});

