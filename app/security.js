// security.js
// handling of credentials and all authentication and access control

// required packages from outside this service
let crypto = require('crypto');
let pino = require('pino')();

// saltForHash: generate salt for hash, return -1 if error
function saltForHash (rounds) {
    if (rounds == null) {
        rounds = 12;
    }
    if (typeof rounds !== 'number') {
        return (-1);
    }
    if (rounds >=15) {
        rounds = 14;
    }
    return crypto.randomBytes(Math.ceil(rounds / 2)).toString('hex').slice(0, rounds);
}

// credential hash: generate hash with credential and salt
function credentialHash (credential, salt) {
    if (credential === undefined || credential === null || credential === '') {
        pino.error('credentialHash got no credential');
        return null;
    }
    if (typeof credential !== 'string') {
        pino.error('credentialHash got non string credential');
        return null;
    }
    if (salt === undefined || salt === null || salt === '') {
        pino.error('credentialHash got no salt');
        return null;
    }
    if (typeof salt !== 'string') {
        pino.error('credentialHash got non string salt');
        return null;
    }
    let hash = crypto.createHmac('sha512', salt);
    hash.update(credential);
    let value = hash.digest('hex');
    return value;
}

// hashCompare returns true if credential matches salted and hashed value
function hashCompare (credential, salt, hash) {
    if (credential === undefined || credential === null || credential === '') {
        pino.error('hashCompare got no credential');
        return null;
    }
    if (typeof credential !== 'string') {
        pino.error('hashCompare got non string credential');
        return null;
    }
    if (salt === undefined || salt === null || salt === '') {
        pino.error('hashCompare got no salt');
        return null;
    }
    if (typeof salt !== 'string') {
        pino.error('hashCompare got non string salt');
        return null;
    }
    if (hash === undefined || hash === null || hash === '') {
        pino.error('hashCompare got no hash');
        return null;
    }
    if (typeof hash !== 'string') {
        pino.error('credentialHash got non string hash');
        return null;
    }
    let credentialData = credentialHash(credential, salt);
    if (credentialData === hash) {
        // match confirmed
        return true;
    }
    // else case
    return false;
}

// define and export object representing module
var security = {}; 
security.saltForHash = saltForHash;
security.credentialHash = credentialHash;
security.hashCompare = hashCompare;
module.exports = security;
