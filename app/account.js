// account.js
// routines supporting account interactions

// internal modules
let config = require('./config');
//let attribute = require('./attribute');
//let step = require('./step');
let permit = require('./permit');
let security = require('./security');
//let persona = require('./persona');

// uuid module needed for new records
let { v4: uuidv4 } = require('uuid');
// log with pino
let pino = require('pino')();

function accountActivate(pgClient, invitationCode, callback) {
    pino.info('account activate: code = %s', invitationCode);
    // take code
    let inspectionQuery = 'SELECT target FROM invitation WHERE code=\'' + invitationCode + '\';';
    pgClient.query(inspectionQuery, function(err, inspectionResult) {
        if (err) {
            pino.error('error looking up correct response: %s', err);
            callback({result: 'failed', reason: 'account-invalid'});
            return;
        }
        // no error, just dump result for now
        // this should generate a temporary guest permit
        // to be used for resetting account password
        // and then ownership of the account
        pino.info('invitation target account: %s', inspectionResult.rows[0].target);
        // !!! get permit before returning !!!
        // ??? {"level":30,"time":1737598955625,"pid":136471,"hostname":"blacksand","msg":"permit create result: {\"result\":\"okay\",\"reason\":\"response-valid\",\"ref\":\"4571dd74-ca2e-41f3-96ee-61c8accd8ae2\"}"}
        permit.create (pgClient, inspectionResult.rows[0].target, 'host', 'agent', 'guest', 'okay', function(permitResult) {
            // ...
            if (permitResult.result === 'failed') {
                pino.error('failed to create permit for activated account: %s', permitResult.reason);
                // not clear what this failure should be called
                callback({result: 'failed', reason: 'authentication-failure'});
                return;
            }
            pino.info('permit create result: %s', JSON.stringify(permitResult));
            callback({result: 'okay', reason: 'invitation processed', permit: permitResult.ref});
            return;
        });
    });
    
    // ???
    // validate code
    // generate permit for account as guest based on code
    // return 
}

function accountSecure(pgClient, unsecuredAccount, securedPermit, callback) {
    pino.info('account secure: account = %s, permit = %s', unsecuredAccount, securedPermit);

    // account and password
    // ??? factor out password assignment function from createAccount?
    // update account with password
    // return result
    
    //            callback({ result: 'done', reason: 'account-created', ref: accountRef });

}

// !!! BELOW THIS IS OLD CODE PRE REWRITE

// accountRequest for creating a new account record
// pgClient is the account database
// accountRequest has account data
//     attributes (list of type, value, qualifier triples),
//     credentials (list of challenge, response pairs)
// callback receives result data
// notes: credentials may be empty,
//     credentials may be added with accountCredential,
//     and account is not completed or live
//     until accountComplete called to set the credentialed timestamp
function accountRequest(pgClient, accountRequest, callback) {
    var accountRef = uuidv4();
    var accountQuery;
    if ((accountRequest.credentials != undefined) && (accountRequest.credentials.length > 0)) {
        // process credentials: generate and store salt and hash
        // thisSalt is used to calculate salt, store that salt, and use salt with hashing
        var thisSalt;
        // challenges, salts, and hashedResponses are strings constructed for use with sql
        var challenges = '\'\{';
        var salts = '\'\{';
        var hashedResponses = '\'\{';
        for (var responseIndex = 0; responseIndex < accountRequest.credentials.length / 2; responseIndex++) {
            if (responseIndex != 0) {
                challenges = challenges + ',';
                salts = salts + ',';
                hashedResponses = hashedResponses + ',';
            }
            challenges = challenges + accountRequest.credentials[responseIndex * 2];
            thisSalt = security.saltForHash(config.saltRounds);
            salts = salts + thisSalt;
            hashedResponses = hashedResponses + 
                security.credentialHash(accountRequest.credentials[responseIndex * 2 + 1], thisSalt);
        }
        challenges = challenges + '\}\'';
        salts = salts + '\}\'';
        hashedResponses = hashedResponses + '\}\'';
        pino.info('account challenges = %s, salts = %s, hashed = %s', challenges, salts, hashedResponses);
        // include credentials in account creation query if present
        accountQuery = 'INSERT INTO account '
            + '(ref,challenge,response,salt,credentialed,personas,roles,status) '
            + 'VALUES (\'' + accountRef + '\','
            + challenges + ',' + hashedResponses + ',' + salts + ',' + 'CURRENT_TIMESTAMP,'
            + '\'\{' + config.defaultPersonaName + '\}\',\'\{member\}\',\'okay\');';
    }
    else {
        // no credentials, so use simplified creation query
        // without credentials and leaving the credentialed time stamp unset
        accountQuery = 'INSERT INTO account '
            + '(ref,personas,roles,status) '
            + 'VALUES (\'' + accountRef + '\','
            + '\'\{' + config.defaultPersonaName + '\}\',\'\{member\}\',\'okay\');';
    }
    // account reference, also needed for account attributes and default persona
    // need to fill out attributes before persona uses them
    // this first draft just spews attribute crud calls
    // but syncronization will probably be required to get this robustly correct
    attribute.post(pgClient, accountRef, accountRequest.attributes, function(result) {
        pino.info('generated account request query: %s', accountQuery);
        // execute query to insert record
        pgClient.query(accountQuery, function (err, result) {
            if (err) {
                pino.error('error creating account record: %s', err);
                callback({result: 'failed', reason: 'account-failed'});
                return;
            }
            callback({ result: 'done', reason: 'account-created', ref: accountRef });
            return;
        });  // end generated account query
    });  // end attribute posting
}  // end function

// accountCredential adds a set of authentication credentials to an account
// note: only accountComplete completes the process of account credentialling
// !!! OUTDATED -- NOW ACCOUNT SECURE INSTEAD
//function accountCredential(pgClient, accountRef, challenge, response, confirmResponse, callback) {
//    let thisSalt = security.saltForHash(config.saltRounds);
//    let hashedResponse = security.credentialHash(response, thisSalt);
//    pino.info('account challenge = %s, salt = %s, hashed response = %s', challenge, thisSalt, hashedResponse);
//    credentialQuery = 'UPDATE account SET challenge = array_append(challenge,\'' 
//    credentialQuery += challenge + '\');';
//    credentialQuery += 'UPDATE account SET response = array_append(response,\'' 
//    credentialQuery += hashedResponse + '\');';
//    credentialQuery += 'UPDATE account SET salt = array_append(salt,\'' 
//    credentialQuery += thisSalt + '\');';
//    pino.info('account credential query: %s', credentialQuery);
//    pgClient.query(credentialQuery, function(err, credentialResult) {
//        if (err) {
//            pino.error('error in credential query: %s', err);
//            callback({result: 'failed', reason: 'failed'});
//            return;
//        }
//        callback({result: 'done', reason: 'credential-added'});
//    });
//}

// accountComplete credentials an account
// by updating the credentialed field with the current timestamp
// OUTDATED
//function accountComplete(pgClient, accountRef, callback) {
//    let completionQuery = 'UPDATE account SET credentialed = CURRENT_TIMESTAMP WHERE ref = \'' + accountRef + '\';';
//    pino.info('account complete: ref = %s, query = %s', accountRef, completionQuery);
//    pgClient.query(completionQuery, function(err, completionResult) {
//        if (err) {
//            pino.error('error completing account with credentialing timestamp: %s', err);
//            callback({result: 'failed', reason: 'account-uncredentialed'});
//            return;
//        }
//        callback({result: 'done', reason: 'account-credentialed'});
//    });
//}

// accountCredentialed returns the date the account was credentialed
// intended to be used as a quick, easy, performant test of account completion
// !!! OUTDATED
//function accountCredentialed(pgClient, accountRef, callback) {
//    let credentialQuery = 'SELECT credentialed FROM account WHERE ref = \'';
//    credentialQuery += accountRef + '\';';
//    pino.info('credential query: %s', credentialQuery);
//    pgClient.query(credentialQuery, function(err, credentialResult) {
//        if (err) {
//            pino.error('account credential query error: %s', err);
//            callback({result: 'failed', reason: 'failed'});
//            return;
//        }
//        callback({result: 'okay', reason: 'account-read', credentialed: credentialResult.rows[0].credentialed});
//    });
//}

// accountReferences takes one name, contact, address, and/or pronoun
// and returns the count and references for the accounts that match
// pgClient is the database
// referenceString is a name or full name
// callback gets the results
// note: reference evaluation does not count as a view of the account record
// !!! unclear exactly how to handle persona with this
// OUTDATED ???
//function accountReferences(pgClient, name, contact, address, pronoun, callback) {
    // first look up by name with exact match
    // !!! should check parameters before giving them to attribute.match
//    attribute.match(pgClient, name, contact, address, pronoun, function(matchResult) {
        // !!! callback should perhaps include other account data 
        // !!! attribute failures should pass through
//        callback({result: matchResult.result, reason: matchResult.reason,
//            account: matchResult.account
//        });
//        return;
//    });
//}

// !!! accountSign REPLACES CHALLENGE AND RESPONSE ... TENTATIVELY
function accountSign(pgClient, xxx, callback) {
}

// accountChallenge returns a challenge needed for authentication
// recording each challenge in steps
// in actual usage get challenge only gets the first challenge
// and the rest are fetched and returned in response processing
//
// pgClient is the database
// name, contact, address, pronoun are arrays of attributes (value, qualifier pairs)
// host and agent are session markers
// and callback gets the result
// !!! using direct ref only instead of ref lookup option
//    that uses attributes name, contact, address, pronoun
// !!! should fail gracefully for missing or uncredentialed accounts
function accountChallenge(pgClient, accountRef, host, agent, callback) {
    // log invocation to be sure parameters are correct
    // compose account query
    credentialQuery = 'SELECT credentialed,challenge,salt FROM account WHERE ref=\'' + accountRef + '\';';
    pgClient.query(credentialQuery, function(err, queryResult) {
        if (err) {
            pino.error('challenge fetch query error %s', err);
            callback({result: 'failed', reason: 'failed'});
            return;
        }
        if (queryResult.rowCount === 0) {
            pino.info('get challenge failed, no such account');
            callback({result: 'failed', reason: 'account-invalid'});
            return;
        }
        if (queryResult.rows[0].credentialed === undefined || queryResult.rows[0].credentialed === null) {
            // account not yet credentialed so fail gracefully
            pino.info('get challenge for uncredentialed account fails');
            callback({result: 'failed', reason: 'account-invalid'});
            return;
        }
        pino.info('challenge query result rows = %s', JSON.stringify(queryResult.rows));
        step.get(pgClient, accountRef, host, agent, function(stepResult) {
            if (stepResult.result === 'okay') {
                // return challenge from stored step
                callback({result: 'okay', reason: 'account-read',
                    challenge: queryResult.rows[0].challenge[stepResult.challenge - 1]});
                return;
            }
            else {
                // step not found so post one and return
                step.post(pgClient, '', accountRef,
                    host, agent, '', 1, function(stepPostResult) {
                    callback({result: 'okay', reason: 'account-read',
                        challenge: queryResult.rows[0].challenge[0]});
                        return;
                });
            }  // end else/step not found block
        });  // end step query block            
    });  // end credential query block
}

// accountResponse processes a response to a challenge
// and returns either failure, another challenge, or a permit
// pgClient is the database
// callback gets the result

// 1. use given account reference (stored since fetch)
// 2. fetch credentials and status: check basics and db errors, bounce if needed
// 3. get step: if none then fail, if step then use index, security hash compare:
//    a. correct response, last challenge: post permit, delete step, return success
//    b. correct response, more challenge: increment step index, return challenge
//    c. incorrect response: return failure
function accountResponse(pgClient, accountRef, response, host, agent, callback) {
    // get account data 
    let responseQuery = 'SELECT ref,challenge,response,salt,roles,status FROM account WHERE ref=\'' + accountRef + '\';';
    pino.info('account response data query: %s', responseQuery);
    pgClient.query(responseQuery, function(err, responseResult) {
        // no responses of any kind found means something went badly wrong
        if (err) {
            pino.error('error looking up correct response %s', err);
            callback({result: 'failed', reason: 'account-unreadable'});
            return;
        }
        pino.info('account response query count: %d, rows: %s', responseResult.rowCount, JSON.stringify(responseResult.rows));
        if (responseResult.rowCount === 0) {
            callback({result: 'failed', reason: 'account-unreadable'});
            return;
        }
        // fail with error if account status banned
        if (responseResult.rows[0].status == 'banned') {
            callback({result: 'failed', reason: 'account-banned'});
            return;
        }
        // fetch current step if any
        step.get(pgClient, accountRef, host, agent, function(stepResult) {
            // number of challenges: responseResult.rows[0].challenge.length
            // current step: stepResult.challenge
            if (stepResult.result != 'okay') {
                // no step found so no challenge cycle in progress
                pino.info('response: no current step found, failing');
                callback({result: 'failed', reason: 'response-invalid'});
                return;
            }
            else {
                pino.info('sign in step data: %s', JSON.stringify(stepResult));
                // use fetched step to get correct expected response and next index
                // no next challenge by default
                let expectedResponse = responseResult.rows[0].response[stepResult.challenge - 1];
                let matchingSalt = responseResult.rows[0].salt[stepResult.challenge - 1];
                // compare response to expectation and take action
                if (security.hashCompare(response, matchingSalt, expectedResponse)) {
                    //if (nextChallenge === 0) { // !!! apparently not?!
                    if (stepResult.challenge === responseResult.rows[0].challenge.length) {
                        // correct response and no more challenges
                        pino.info('response: correct with no more challenges');
                        permit.post(pgClient, responseResult.rows[0].ref,
                            host, agent, 
                            responseResult.rows[0].role, responseResult.rows[0].status,
                            function(permitPostResult) {
                            //pino.info('new permit saved, returning success'); 
                            step.delete(pgClient, stepResult.ref, function() {
                                //pino.info('credential check steps cleared');
                                callback({result: 'okay', reason: 'response-valid',
                                    permit: permitPostResult.ref,
                                    ref: responseResult.rows[0].ref});  //should be 'account'?
                                return;
                            });
                       });
                    } 
                    else {
                        // correct response but other challenges remain
                        // prepare challenge string
                        // increment step first
                        pino.info('response correct but challenges remain');
                        pino.info('step: %d, challenges: %s', stepResult.challenge, JSON.stringify(responseResult.rows[0].challenge));
                        // update step challenge index
                        // note steps start at 1 but arrays and indexes start at 0
                        step.post(pgClient, stepResult.ref, account, host, agent, '', stepResult.challenge + 1, function() {
                            callback({result: 'okay', reason: 'next-challenge', challenge: responseResult.rows[0].challenge[stepResult.challenge]});
                            return;
                        });
                    }
                }
                else {
                    // response invalid: send notification and fail
                    pino.info('response is incorrect');
                    callback({result: 'failed', reason: 'response-invalid'});
                    return;
                }
            }        
        });  // end step challenge check query
    });  // end database correct response hash query block
}

// accountInspect fetches account data
// the idea is to take any set of references that finds a unique record
// as with accountChallenge
// but for now it still simply looks up the name and ignores the rest
// pgClient is the database
// accountRecord has account reference info, name and permit for now
// callback gets the result
function accountInspect(pgClient, accountRef, permitRef, personaName, callback) {
    let usage = 'read';
    permit.check(pgClient, accountRef, permitRef, usage, function(checkResult) {
        if (checkResult.result != 'okay') {
            //pino.info('invalid permit, inspection failed');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // fetch account data, attributes, then return almost everything
        //status, roles - special handling, text and array
        //challenge - challenge response pairs, credentials specially handled
        //name, contact, address, pronoun - array of value and qualifier pairs
        // this is a double query that gets the info and sets the viewed field
        // and so the result will be a pair of results in an array
        let inspectionQuery = 'SELECT personas,roles,status,created,viewed,modified'
            + ' FROM account WHERE ref=\'' + accountRef + '\';'
            + 'UPDATE account SET viewed = CURRENT_TIMESTAMP'
            + ' WHERE ref=\'' + accountRef + '\'';
        pgClient.query(inspectionQuery, function(err, inspectionResult) {
            if (err) {
                pino.error('error looking up correct response: %s', err);
                callback({result: 'failed', reason: 'account-invalid'});
                return;
            }
            if (inspectionResult[0].rowCount === 0) {
                //pino.info('account not found: %S', accountRecord.name);
                callback({result: 'failed', reason: 'account-invalid'});
                return;
            }
            attribute.get(pgClient, accountRef, '', function(attributeResult) {
                pino.info('attribute get/fetch result: %s', JSON.stringify(attributeResult));
                // all information now collected so call back and done
                // collect and print all info before calling callback
                callback({result: 'okay', reason: 'account-read',
                    personas: inspectionResult[0].rows[0].personas,
                    roles: inspectionResult[0].rows[0].roles,
                    status: inspectionResult[0].rows[0].status,
                    created: inspectionResult[0].rows[0].created,
                    credentialed: inspectionResult[0].rows.credentialed,
                    viewed: inspectionResult[0].rows[0].viewed,
                    modified: inspectionResult[0].rows[0].modified,
                    // attributes as array of (type,value,qualifier) triples
                    attributes: attributeResult.attributes
                });
                return;
            });  // end attribute fetch block
        });  // end inspection query block
    }); // end permit check block
}

// accountUpdate writes arbitrary fields to account records given a permit
// pgClient is the database
// accountRecord has updated fields which may include name but not ref
//     and has attributes in pair arrays
// callback gets results
function accountUpdate(pgClient, accountRecord, attributes, callback) {
    // check if permit valid and allows operation
    permit.check(pgClient, accountRecord.account, accountRecord.permit, 'write', function(checkResult) {
        if (checkResult.result != 'okay') {
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // permit checked out, now compose account update query
        var updateQuery = 'UPDATE account SET modified = CURRENT_TIMESTAMP ';
        if (accountRecord.roles != undefined && accountRecord.roles != '') {
            updateQuery = updateQuery + ', roles = \''
                + JSON.stringify(accountRecord.roles) + '\' ';
        }
        updateQuery = updateQuery + 'WHERE ref=\'' + accountRecord.account + '\' ';
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating account: %s', err);
                callback({result: 'failed', reason: 'account-update-failed'});
                return;
            }
            attribute.post(pgClient, accountRecord.account, attributes, function(result) {
                // success! account record and attributes updated, so reurn result
                callback({result: 'done', reason: 'account-updated'});
                return;
            });            
        });  // end account update query block
    });  // end permit check block
}

// generate and return a string representing this account
// consists of name and address entries
//    account.summary(req.body.ref, function(result) 
// !!! stub only, not implemented
// !!! OUDATED
//function accountSummary() {
    // ... 
    
//}

// !!! THIS ROUTINE NEVER COMPLETED, INCOMPLETE CODE PARTIALLY REFACTORED
// accountReset triggers replacement of account credentials
// pgClient is the database
// accountRecord has account data
// host and agent keep multiple steps together
// callback gets the result
// reset process has three steps and does not require permit:
// 1. initiate reset: creates reset step with code
// 2. return code as sent: modifies step indicating code return
// 3. send new credentials: test host/agent match, make change, delete step
// !!! OUTDATED
function accountReset(pgClient, accountRecord, code, host, agent, callback) {
    // check reset step
    // not present means initiation so make and save code in new step
    // step present means new call with correct code completes reset
    
    // !!! need to first get account ref from account name if needed
    //pino.info('account reset, code = %s, host = %s, agent = %s', code, host, agent);
    step.get(pgClient, accountRecord.ref, host, agent, function(stepFetch) {
        if (stepFetch.result === 'okay') {
            if (code === stepFetch.code) {
                // step with matching code
                // use of '0' as a magic code is questionable--define it first
                step.post(pgClient, accountRecord.ref, host, agent, '0', 0, function() {
                    //pino.info('reset code received and approved');
                    callback({result: 'done', reason: 'account-reset-permitted'});
                    return;
                });
            }
            else if (code === '0') {
                // matching step but no code means final reset credentials set
                // double check and receive new credentials
                var resetQuery = 'UPDATE ';
                pgClient.query(resetQuery, function(err) {
                    if (err) {
                        pino.error('error resetting account: %s', err);
                        callback({result: 'failed', reason: 'account-reset-failed'});
                        return;
                    }
                    // !!! must also clear any permits issued with old credentials
                    //     possibly add interfaces to permit for easy clearing?
                    step.delete(pgClient, accountRecord.ref, function() {
                        //pino.info('reset process complete');
                        callback({result: 'done', reason: 'account-reset-complete'});
                        return;
                    });
                });  // end reset query block
            }
            else {
                // fail with non matching code
                //pino.info('failed, reset code does not match');
                callback({result: 'failed', reason: 'reset-code-incorrect'});
                return;
            }
        }
        else {
            // no reset process active, so start one
            // this means creating a code,
            // storing that code in a step,
            // and sending the code out as appropriate
            var resetCode = 0;
            step.post(pgClient, '', accountRecord.ref, host, agent, resetCode, 0, function() {
                //pino.info('initiating reset process, first step');
                // reason code is in question--potentially three cases
                callback({result: 'done', reason: 'account-reset-started'});
                return;
            });
        }
    });  // end step fetch block
}

// accountDelete advances multi step deletion process, requires confirmation
// pgClient is database
// account and permit are references to these structures, both required
// host and agent keep multiple steps associated
// callback gets result
// !!! TENTATIVELY OUTDATED
function accountDelete(pgClient, accountRef, permitRef, host, agent, callback) {
    let usage = 'ownership';
    permit.check(pgClient, accountRef, permitRef, usage, function(checkResult) {
        if (checkResult.result != 'okay') {
            //pino.info('invalid permit, inspection failed');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // permit is okay
        // check for step indicating deletion in progress
        step.get(pgClient, accountRef, host, agent, function(confirmStep) {
            if (confirmStep.result != 'okay') {
                // no deletion initiated confirmation step, so create one
                // used to be type=confirmation, now no such record retained?
                step.post(pgClient, confirmStep.ref, accountRef, host, agent, '', 1, function() {
                    callback({result: 'okay', reason: 'deletion-initiated'});
                    return;
                });
            }  // end create deletion confirmation step block
            else {
                // found deletion initiated confirmation step, so proceed
                let deleteQuery = 'DELETE FROM account WHERE ref=\'' 
                    + accountRef + '\';';
                pgClient.query(deleteQuery, function (err) {
                    if (err) {
                        pino.error('error running postgres query: %s', err);
                        callback({result: 'failed', reason: 'failed'});
                        return;
                    }
                    attribute.delete(pgClient, accountRef, '', '', '', function(attributeResult) {
                        // check for error and proceed
                        // having deleted account and attributes
                        // remove the confirmation step
                        step.delete(pgClient, confirmStep.ref, function() {
                            // no alternative, so ignore result?
                            // actions complete, return result
                            callback({result: 'done', reason: 'account-deleted'});
                            return;
                        });  // end confirmation step clearing query block 
                    });  // end attribute deletion block
                });  // end account deletion query block
            }  // end deletion confirmed so perform deletion block
        });  // end confirmation check block
    }); // end permit check block
}

// object to be exported by module
var account = {};  
account.activate = accountActivate;
account.secure = accountSecure;
account.request = accountRequest;
//account.credential = accountCredential;
//account.credentialed = accountCredentialed;
//account.complete = accountComplete;
//account.references = accountReferences;
//account.challenge = accountChallenge;
//account.response = accountResponse;
account.inspect = accountInspect;
account.update = accountUpdate;
//account.summary = accountSummary;
//account.reset = accountReset;
//account.delete = accountDelete;
module.exports = account;
