// step.js
// basic support for multiple step processes
// including authorization, contact checks, deletion verification

// uuid module needed for new records
let { v4: uuidv4 } = require('uuid');
// log with pino
let pino = require('pino')();

// stepFetch gets step data given account reference, host, and agent
// fails if there is no match or by invalid state multiple matches found
function stepFetch(pgClient, accountRef, host, agent, callback) {
    let stepQuery = 'SELECT ref,challenge,code FROM step WHERE '
        + 'account = \'' + accountRef + '\' '
        + 'AND host = \'' + host + '\' '
        + 'AND agent = \'' + agent + '\';';
    pgClient.query(stepQuery, function (err, stepResult) {
        if (err) {
            pino.error('step query error: %s', err);
            callback({result: 'failed', reason: 'invalid-data'});
            return;
        }
        if (stepResult.rowCount === 1) {
            // exactly one step found, return its data
            callback({result: 'okay', reason: 'step-read', 
                ref: stepResult.rows[0].ref,
                code: stepResult.rows[0].code, 
                challenge: stepResult.rows[0].challenge
            });
            return;
        }
        else {
            // zero or multiple matching steps found means failure
            callback({result: 'failed', reason: 'no-valid-match'});
            return;
        }
    });
}

// stepPost updates existing step if ref is given
// otherwise creates a new step from given data
function stepPost(pgClient, ref, accountRef, host, agent, code, challenge, callback) {
    if (ref === undefined || ref === '') {
        // no reference given means create a new record
        let codeQuery = 'INSERT INTO step (ref, account, host, agent, code, challenge)'
            + ' VALUES (\''
            + uuidv4() + '\', \''
            + accountRef + '\', \''
            + host + '\', \''
            + agent + '\', \''
            + code + '\', \''
            + challenge + '\');';
        pgClient.query(codeQuery, function (err) {
            if (err) {
                pino.error('error creating confirmation step: %s', err);
                callback({result: 'failed', reason: 'failed'});
                return;
            }
            // no error, return success
            callback({result: 'done', reason: 'step-written'});
            return;
        });
    }
    else {
        // given a reference means update that record
        // this current formulsation updates the challenge only and always
        var updateStepQuery = 'UPDATE step SET challenge = \''
            + challenge + '\'  WHERE ref = \'' + ref + '\';';
        pgClient.query(updateStepQuery, function(err) {
            if (err) {
                pino.error('error updating step: %s', err);
                callback({result: 'failed', reason: 'failed'});
                return;
            }
            callback({result: 'okay', reason: 'step-updated', 
                code: code, challenge: challenge});
            return;
        });
    }
}

function stepDelete(pgClient, ref, callback) {
    let deleteStepQuery = 'DELETE FROM step WHERE ref = \'' + ref + '\';';
    pgClient.query(deleteStepQuery, function(err) {
        if (err) {
            pino.error('error deleting step: %s', err);
            callback({result: 'failed', reason: 'failed'});
            return;
        }
        callback({result: 'done', reason: 'step-deleted'});
        return;
    });
}

// object to be exported by module
var step = {};  
step.get = stepFetch;
step.post = stepPost;
step.delete = stepDelete;
module.exports = step;
