// account service configuration
var config = {};

// database access string
config.dbAccessString = 'postgres://tender:tdr@localhost:5432/account';

// port for service
config.servicePort = '51112';

// rounds for salt
config.saltRounds = 8;

// large limit for credential size
config.credentialSizeLimit = 10000;

// default persona name is a code for internal use only
// to be displayed using localization
config.defaultPersonaName = 'DEFAULT_PERSONA';

// hand off data as required
module.exports = config;
