// persona.js
// support for defining filters for account data based on context
// !!! persona feature has not been completed

// internal modules
let config = require('./config');
let attribute = require('./attribute');
let step = require('./step');
let permit = require('./permit');
let security = require('./security');

// log with pino
let pino = require('pino')();
// pesona records all get v4 uuids
let { v4: uuidv4 } = require('uuid');

// request persona for filtering account data
// pgClient is database
// account and permit are used to check authorization and mark record
// array of attribute qualifiers for name, contact, address, pronoun
// and a boolean switch for the notes field
// callback gets report of result including direct reference labeled "persona"
function personaRequest(pgClient, accountRef, permitRef, personaName, callback) {
    pino.info('requesting persona with name = %s', personaName); // max debug spew rawr
    // check permit
    let usage = 'write';
    permit.check(pgClient, accountRef, permitRef, usage, function (checkResult) {
        if (checkResult.result != 'okay') {
            pino.info('invalid permit, persona request denied');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let personasQuery = 'SELECT personas FROM account WHERE ref=\'' + accountRef + '\';';
        pino.info('personas query: %s', personasQuery);
        pgClient.query(personasQuery, function(err, personasResult) {
            if (err) {
                pino.error('persona query failed, %s', err);
                callback({result: 'failed', reason: 'persona-invalid'});
                return;
            }
            // first check that result is a nonempty array as expected?
            pino.info('current personas: %s', JSON.stringify(personasResult.rows[0].personas));
            // check for duplicates in string
            for (var personaIndex = 0; personaIndex < personasResult.rows[0].personas.length; personaIndex++) {
                if (personaName === personasResult.rows[0].personas[personaIndex]) {
                    // conflict, report error and fail
                    pino.info('persona name conflict at index = %d', personaIndex);
                    callback({result: 'failed', reason: 'name-conflict'});
                    return;
                }
            }
            // reaching here means okay to append new persona to list
            let appendQuery = 'UPDATE account SET personas = array_append(personas,\'' + personaName + '\') WHERE ref=\'' + accountRef + '\';';
            pino.info('append query = %s', appendQuery);
            pgClient.query(appendQuery, function(err, appendResult) {
                if (err) {
                    pino.error('persona append failed: %s', err);
                    callback({result: 'failed', reason: 'append-failed'});
                    return;
                }
                // return success, possibly also include direct reference
                callback({result: 'okay', reason: 'persona-added'});
            });  // end append query block
        });  // end persona query and conflict check block
    });  // end permit check block
}

// inspect persona data
function personaInspect(pgClient, accountRef, permitRef, personaName, callback) {
    let usage = 'read';
    permit.check(pgClient, accountRef, permitRef, usage, function (checkResult) {
        if (checkResult.result != 'okay') {
            pino.info('invalid permit, persona request denied');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // persona string reference is assumed correct
        //     so no need to fetch the personas list from account
        // initial implementation recalls all persona data and reports
        //     but ideally should cross reference with account attributes
        //     in order to skip or at least indicate orphaned entries
        //     that is persona entries for attributes that have been deleted
        // view count updating: 'UPDATE account SET viewed = CURRENT_TIMESTAMP '
        //     + 'WHERE ref=\'' + accountRef + '\';'
        let personaQuery = 'SELECT attribute,value,qualifier FROM persona WHERE account=\'' + accountRef + '\' AND persona=\'' + personaName + '\';';
        pino.info('persona inspection query: %s', personaQuery);
        pgClient.query(personaQuery, function(err, personaResult) {
            if (err) {
                pino.error('persona inspection query error: %s', err);
                callback({result: 'failed', reason: 'persona-invalid'});
                return;
            }
            pino.info('persona inspection count: %d', personaResult.rowCount);
            pino.info('persona inspection rows: %s', JSON.stringify(personaResult.rows));
            if (personaResult.rowCount > 0) {
                pino.info('persona inspection attr: %s', personaResult.rows[0].attribute);
            }
            // package and return result
            var personaRecords = [];
            for (var recordIndex = 0; recordIndex < personaResult.rowCount; recordIndex++) {
                personaRecords[recordIndex] = {};  // object must exist before setting records
                personaRecords[recordIndex].attribute = personaResult.rows[recordIndex].attribute;
                personaRecords[recordIndex].value = personaResult.rows[recordIndex].value;
                personaRecords[recordIndex].qualifier = personaResult.rows[recordIndex].qualifier;
            }
            callback({result: 'okay', reason: 'persona-read', records: personaRecords});
            return;
        });
    });
}

// check persona inspects one personoid data field of persona
function personaCheck(pgClient, accountRef, permitRef, qualifier, callback) {
    let usage = 'read';
    permit.check(pgClient, accountRef, permitRef, usage, function (checkResult) {
        if (checkResult.result != 'okay') {
            pino.info('invalid permit, persona request denied');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let personoidQuery = 'SELECT attribute,text,qualifier FROM personoid WHERE account=\'' + accountRef + '\' AND persona=\'' + personaString + '\';';
        pgClient.query(personoidQuery, function(err, personoidResult) {
            if (err) {
                pino.error();
                callback({result: 'failed', reason: ''});
                return;
            }
            // package and return result
            callback({result: '', reason: '', xxx: ''});
            return;
            
        });  // end query block
    });  // end permit check block
}

// persona records for an account fetched all together
function personaRecords(pgClient, accountRef, permitRef, callback) {
    let usage = 'read';
    permit.check(pgClient, accountRef, permitRef, usage, function(checkResult) {
        if (checkResult.result != 'okay') {
            pino.info('invalid permit, persona request denied');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let recordsQuery = 'SELECT persona,attribute,value,qualifier FROM persona WHERE account=\'' + accountRef + '\';';
        pino.info('persona records query: %s', recordsQuery);
        pgClient.query(recordsQuery, function(err, recordsResult) {
            if (err) {
                pino.error('persona records query failed: %s', err);
                callback({result: 'failed', reason: 'invalid-persona'});
                return;
            }
            // package and return result
            callback({result: 'okay', reason: 'persona-read', count: recordsResult.rowCount, records: recordsResult.rows});
            return;
        });
    });  // end permit check block
}
  
// update an existing persona
// personaUpdate takes essentially the same arguments as personaRequest
// this means that in order to correctly implement deletions
// all name, contact, address, and pronoun information must be complete
// and will replace any previous such data
// and if no such data is given then the result will be empty
function personaUpdate(pgClient, accountRef, permitRef, personaRecord, callback) {
    let usage = 'write';
    permit.check(pgClient, accountRef, permitRef, usage, function (checkResult) {
        if (checkResult.result != 'okay') {
            pino.info('invalid permit, persona request denied');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // !!! refactoring in progress from here
        var personaFields = '';
        var personaValues = '';
        // name, contact, address, pronoun are all arrays of qualifier strings
        // and arrays in sql are delimited by curly braces
        if (personaRecord.name != undefined) {
            personaFields = personaFields + ',name';
            personaValues = personaValues + ',\'\{';
            for (nameIndex = 0; nameIndex < personaRecord.name.length; nameIndex++) {
                if (nameIndex != 0) {
                    personaValues = personaValues + ',';
                }
                personaValues = personaValues //+ '\'' 
                    + personaRecord.name[nameIndex];// + '\'';
            }
            personaValues = personaValues + '\}\'';
        }
        if (personaRecord.contact != undefined) {
            personaFields = personaFields + ',contact';
            personaValues = personaValues + ',\'\{';
            for (contactIndex = 0; contactIndex < personaRecord.contact.length; contactIndex++) {
                if (contactIndex != 0) {
                    personaValues = personaValues + ',';
                }
                personaValues = personaValues //+ '\'' 
                    + personaRecord.contact[contactIndex];// + '\'';
            }
            personaValues = personaValues + '\}\'';
        }
        let updateQuery = 'UPDATE persona '
            + '(ref,account,qualifier' + personaFields + ') '
            + 'VALUES (\'' + personaRef + '\',\'' + accountRef + '\',\''
            + personaRecord.qualifier + '\'' + personaValues + ');';
        pino.info('persona request query: %s', JSON.stringify(updateQuery));
        pgClient.query(updateQuery, function(err, updateResult) {
            if (err) {
                pino.error('persona request failed, %s', err);
                callback({result: 'failed', reason: 'persona-invalid'});
                return;
            }
            // success return is okay because this is only a request
            callback({result: 'okay', reason: 'persona-requested', 
                persona: personaRef});
            return;
        });  // end persona query block    
        // !!! implementation in progress -- return fail
    });  // end permit check block
}

// personaToggle is for making incremental changes to a persona
// one specific record at a time
//
// this pedantically checks the value before taking action
// expected usage is triggered from user interface
// so there should be no reason to check for double positive or negative
// except in case of bugs or maybe dual sign ins in conflict
//
// pgClient is database access
// accountRef is direct account ref (account is module name)
// permitRef is direct permit ref (permit is module name)
// personaName is text for persona
// attributeName is text of attribute name/type
// value is the actual text of the attribute
// qualifier notes usage of attribute
// state is true or false indicating active or inactive
// callback gets called with the result
function personaToggle(pgClient, accountRef, permitRef, personaName, attributeName, value, qualifier, state, callback) {
    let usage = 'write';
    permit.check(pgClient, accountRef, permitRef, usage, function (checkResult) {
        if (checkResult.result != 'okay') {
            pino.info('invalid permit, persona request denied');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let stateQuery = 'SELECT ref FROM persona '
            + 'WHERE account = \'' + accountRef 
            + '\' AND persona = \'' + personaName 
            + '\' AND attribute = \'' + attributeName
            + '\' AND value = \'' + value
            + '\' AND qualifier = \'' + qualifier
            + '\';';
        pino.info('persona state query: %s', stateQuery);
        pgClient.query(stateQuery, function(err, stateResult) {
            if (err) {
                pino.error('error in persona state query: %s', err);
                callback({result: 'failed', reason: 'invalid-persona'});
                return;
            }
            pino.info('state query: state = %s, row count = %d', JSON.stringify(state), stateResult.rowCount);
            if (state && stateResult.rowCount != 1) {
                // activate persona field, only take action if not already set
                // personaToggle(pgClient, accountRef, permitRef, personaName, attributeName, value, qualifier, state, callback)
                let personaRef = uuidv4();
                let insertionQuery = 'INSERT INTO persona (ref,account,persona,attribute,value,qualifier) VALUES (\''
                    + personaRef + '\',\''
                    + accountRef + '\',\''
                    + personaName + '\',\''
                    + attributeName + '\',\''
                    + value + '\',\''
                    + qualifier + '\');';
                pino.info('persona insertion query: %s', insertionQuery);
                pgClient.query(insertionQuery, function(err, insertionResult) {
                    if (err) {
                        pino.error('error: %s', err);
                        callback({result: 'failed', reason: 'invalid-persona'});
                        return;
                    }
                    // ...
                    callback({result: 'done', reason: 'persona-append', ref: personaRef});
                    return;
                });
            }
            else if (stateResult.rowCount === 1) {
                // deactivate persona field, only take action if found
                let deletionQuery = 'DELETE FROM persona WHERE ref=\'' 
                    + stateResult.rows[0].ref + '\';';
                pino.info('persona deletion query: %s', deletionQuery);
                pgClient.query(deletionQuery, function(err, deletionResult) {
                    if (err) {
                        pino.error('error: %s', err);
                        callback({result: 'failed', reason: 'invalid-persona'});
                        return;
                    }
                    callback({result: 'done', reason: 'persona-trim'});
                    return;
                });
            }
        });  // end state query block
    });  // end permit check block
}


// list all personas for given account
function personaList(pgClient, accountRef, permitRef, callback) {
    let usage = 'read';
    permit.check(pgClient, accountRef, permitRef, usage, function (checkResult) {
        if (checkResult.result != 'okay') {
            pino.info('invalid permit, persona request denied');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let personasQuery = 'SELECT personas FROM account WHERE ref=\'' + accountRef + '\';';
        pino.info('personas query: %s', personasQuery);
        pgClient.query(personasQuery, function(err, personasResult) {
            if (err) {
                pino.error('persona query failed, %s', err);
                callback({result: 'failed', reason: 'persona-invalid'});
                return;
            }
            // successful fetch, so report personas--confirm result before returning
            pino.info('persona query result: %s', JSON.stringify(personasResult.rows[0].personas));
            callback({result: 'okay', reason: 'personas-read', personas: personasResult.rows[0].personas});
            return;
        });  // end personas fetch block
    });  // end permit check block
}

// delete persona and any related storage records
function personaDelete(pgClient, accountRef, permitRef, personaName, callback) {
    pino.info('deleting persona: %s', personaName);
    let usage = 'write';
    permit.check(pgClient, accountRef, permitRef, usage, function (checkResult) {
        if (checkResult.result != 'okay') {
            pino.info('invalid permit, persona request denied');
            callback({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let deletionQuery = 'UPDATE account SET personas = array_remove(personas,\'' + personaName + '\') WHERE ref=\'' + accountRef + '\';';
        pino.info('deletion query: %s', deletionQuery);
        pgClient.query(deletionQuery, function(err, deletionResult) {
            if (err) {
                pino.error('persona query failed, %s', err);
                callback({result: 'failed', reason: 'persona-invalid'});
                return;
            }
            callback({result: 'done', reason: 'persona-deleted'});
            return;            
        });  // personas something block
    });  // end permit check block
}

// object to be exported by module
var persona = {};  
persona.request = personaRequest;
persona.inspect = personaInspect;
persona.check = personaCheck;
persona.records = personaRecords;
persona.update = personaUpdate;
persona.toggle = personaToggle;
persona.list = personaList;
persona.delete = personaDelete;
module.exports = persona;
