// attribute.js
// basic support for attributes of accounts
// which have a type of name, contact, address, pronoun, notes
// and content being value and qualifier
//
// note that attribute methods are used internally
// from account or persona methods
// and so do not need to check permissions 

// uuid module needed for new records
let { v4: uuidv4 } = require('uuid');
// log with pino
let pino = require('pino')();

// attributePost used for new and updates
// pgClient is the database
// account is a direct reference
// attributes is an array of type, value, qualifier strings
// callback gets the result
function attributePost(pgClient, accountRef, attributes, callback) {
    // build multiple row insertion query
    // this must start with a deletion of all potential conflicts
    // which is any match of account ref, type, and qualifier
    // but first implementation simply ignores actual and potential conflicts
    var insertionQuery = 'INSERT INTO attribute (account,type,value,qualifier) VALUES ';
    for (var attributeIndex = 0; attributeIndex < attributes.length / 3; attributeIndex++) {
        insertionQuery = insertionQuery + '(\'' + accountRef + '\',\''
            + attributes[attributeIndex * 3] + '\',\''
            + attributes[attributeIndex * 3 + 1] + '\',\''
            + attributes[attributeIndex * 3 + 2] + '\')';
        if (attributeIndex === (attributes.length / 3) - 1) {
            insertionQuery = insertionQuery + ';';
        }
        else {
            insertionQuery = insertionQuery + ',';
        }
    }  // end attribute loop
    pgClient.query(insertionQuery, function (err, insertionResult) {
        if (err) {
            pino.error('attribute post error: %s', err);
            callback({result: 'failed', reason: 'invalid-data'});
            return;
        }
        callback({result: 'done', reason: 'attribute-posted'});
        return;
    });  // end insertion query block
}  // end post attribute function block

// attribute gets all such attributes for an account
// returns an array which may be undefined, empty, or singular
// pgClient is the database
// account is a direct reference
// attribute is a string: name, contact, address, pronoun
// callback gets the result in two arrays: value and qualifier
function attributeFetch(pgClient, accountRef, attributeType, callback) {
    let triples = [];
    let fetchQuery = 'SELECT type,value,qualifier FROM attribute WHERE '
        + 'account = \'' + accountRef + '\' ';
    if (attributeType != undefined && attributeType != '') {
        fetchQuery = fetchQuery + 'AND type = \'' + attributeType + '\';';
    }
    else {
        fetchQuery = fetchQuery + 'GROUP BY type,value,qualifier;'
    }
    pgClient.query(fetchQuery, function (err, fetchResult) {
        if (err) {
            pino.error('attribute query error: %s', err);
            callback({result: 'failed', reason: 'invalid-data'});
            return;
        }
        // if no match then return immediately with zero count
        if (fetchResult.rowCount === 0) {
            callback({result: 'okay', reason: 'attribute-read', count: 0});
            return;
        }
        else {
            // fill value and qualifier arrays
            for (var matchIndex = 0; matchIndex < fetchResult.rowCount; matchIndex++) {
                // type, value, qualifier
                triples[matchIndex * 3]  = fetchResult.rows[matchIndex].type;
                triples[matchIndex * 3 + 1] = fetchResult.rows[matchIndex].value;
                triples[matchIndex * 3 + 2] = fetchResult.rows[matchIndex].qualifier;
            }
            callback({result: 'okay', reason: 'attribute-read',
                count: fetchResult.rowCount, attributes: triples});
            return;
        }  // end nonzero row count block
    });  // end fetch query block
}

// attributeMatch is used to find references to potentially matching accounts
// note that name, contact, pronoun matches should be exact
// but address matches should count if given string is present (wildcarded match)
// pgClient is the database
// attribute is a string: name, contact, address, pronoun
// value is the expected value string
// callback gets the result
// result is a count and array of account references that matched
function attributeMatch(pgClient, name, contact, address, pronoun, callback) {
    let matchQuery = '';
    let factorCount = 0;
    if (name != undefined && name != '') {
        matchQuery += 'SELECT account FROM attribute ';
        matchQuery += 'WHERE (type=\'name\' AND value=\'' + name + '\');';
        factorCount++;
    }
    if (contact != undefined && contact != '') {
        matchQuery += 'SELECT account FROM attribute ';
        matchQuery += 'WHERE (type=\'contact\' AND value=\'' + contact + '\');';
        factorCount++;
    }
    if (address != undefined && address != '') {
        matchQuery += 'SELECT account FROM attribute ';
        matchQuery += 'WHERE (type=\'address\' AND value=\'' + address + '\');';
        factorCount++;
    }
    if (pronoun != undefined && pronoun != '') {
        matchQuery += 'SELECT account FROM attribute ';
        matchQuery += 'WHERE (type=\'pronoun\' AND value=\'' + pronoun + '\');';
        factorCount++;
    }
    pino.info('attribute match query: %s', matchQuery);
    if (factorCount === 0) {
        // no factors means no match, return failure without doing anything
        callback({result: 'failed', reason: 'invalid'});
        return;
    }
    pino.info('factor count: %d', factorCount);
    pgClient.query(matchQuery, function (err, matchResult) {
        if (err) {
            pino.error('name attribute match query error %s', err);
            callback({result: 'failed', reason: 'failed', count: 0});
            return;
        }
        // !!! zero matches case not correctly handled up front
        if (factorCount === 1) {
            // only one factor, so any match found is the one and any other is failure
            if (matchResult.rowCount == 1) {
              pino.info('one factor, one match found, success');
              callback({result: 'okay', reason: 'account-found', account: matchResult.rows[0].account});
              return;
            }
            else {
              pino.info('zero or multiple matches found, failure');
              if (matchResult.rowCount == 0) {
                  callback({result: 'failed', reason: 'not-found'});
              }
              else {
                  callback({result: 'failed', reason: 'multiple-matches'});
              }
              return;
            }
        }
        // candidates are account matches for all match results
        // match only valid if only one ultimately matches all results
        // method: record first set of matches as candidates
        // then eliminate all candidates not in later match lists
        var candidates = [];
        for (var matchIndex = 0; matchIndex < matchResult.length; matchIndex++) {
            pino.info('attribute/factor match rows: %s', JSON.stringify(matchResult[matchIndex].rows));
            // any empty match result means no account matched this attribute/factor, so fail
            if (matchResult[matchIndex].rowCount === 0) {
                pino.info('no matches at all, so account not found');
                callback({result: 'failed', reason: 'not-found'});
                return;
            }
            var matchList = [];
            for (var accountIndex = 0; accountIndex < matchResult[matchIndex].rowCount; accountIndex++) {
                pino.info('considering match account %s', matchResult[matchIndex].rows[accountIndex].account);
                if (matchIndex == 0) {
                    // first round/attribute/factor all account matches are candidates
                    pino.info('first match set candidate: %s', matchResult[matchIndex].rows[accountIndex].account);
                    candidates.push(matchResult[matchIndex].rows[accountIndex].account);
                }
                else {
                    // matches in later rows assembled for comparison
                    matchList.push(matchResult[matchIndex].rows[accountIndex].account);
                }
            }  // end for accounts in match set loop block
            pino.info('match list: %s', JSON.stringify(matchList));
            if (matchIndex != 0) {
                reviewedCandidates = [];
                for (candidateIndex = 0; candidateIndex < candidates.length; candidateIndex++) {
                    if (matchList.includes(candidates[candidateIndex]) ) {
                        reviewedCandidates.push(candidates[candidateIndex]);
                    }
                }
                pino.info('reviewed candidates: %s', JSON.stringify(reviewedCandidates));
                if (reviewedCandidates.length === 0) {
                    pino.info('all candidates eliminated, account not found');
                    callback({result: 'failed', reason: 'not-found'});
                    return;
                }
                // update the list of candidates as reviewed and proceed
                candidates = reviewedCandidates;
            }
        }  // end for match set from query loop block
        // reaching this point means list of candidates is either one or too many
        if (candidates.length === 1) {
            // one remaining candidate must be a good match
            callback({result: 'okay', reason: 'account-found', account: candidates[0]});
            return;
        }
        else {
            // multiple remaining candidates indicates failure
            callback({result: 'failed', reason: 'multiple-matches'});
            return;
        }
    });  // end attribute match query block
}


// attributeDelete is used to delete all such attributes from an account
// note that attributePost should be used to nullify individual value, qualifier pairs
// pgClient is the database
// account is a direct reference
// attribute is a string: name, contact, address, pronoun
// text and qualifier are optional details of specific attribute to delete
//     if not given then all such attributes are deleted together
// callback gets the result which is done or failed
// !!! change to enable individual delete with specific references
//     and fall back on all attribute coverage if no specifics given
function attributeDelete(pgClient, account, type, value, qualifier, callback) {
    //        attribute.delete(pgClient, req.body.account, req.body.type, req.body.value, req.body.qualifier, function(result) {
    // type value qualifier INSTEAD OF attribute text qualfier

    // set up basic deletion query
    var deleteQuery = 'DELETE FROM attribute WHERE '
        + 'account = \'' + account + '\'';
    if (type != undefined && type != '' ) {
        deleteQuery = deleteQuery + ' AND type = \'' + type + '\'';
    }
    if (value != undefined && value != '') {
        deleteQuery = deleteQuery + ' AND value = \'' + value + '\'';
    }
    if (qualifier != undefined && qualifier != '') {
        deleteQuery = deleteQuery + ' AND qualifier = \'' + qualifier + '\';';
    }
    // terminate query, log query text, then execute query
    deleteQuery = deleteQuery + ';';
    pgClient.query(deleteQuery, function(err) {
        if (err) {
            pino.error('attribute deletion error: %s', err);
            callback({result: 'failed', reason: 'failed'});
            return;
        }
        callback({result: 'done', reason: 'attribute-deleted'});
        return;
    });
}

var attribute = {};
attribute.post = attributePost;
attribute.get = attributeFetch;
attribute.match = attributeMatch;
attribute.delete = attributeDelete;
module.exports = attribute;

