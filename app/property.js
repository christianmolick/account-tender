// property.js
// basic support for fetching server operating parameters
//
// !!! originally expected to store options and parameters
// this is only used for name, version, and description
// which are primarily stored in the package.json file
// so this may need to be refactored to not use database tables

// propertyFetch function returns named values from table
// propertyName is the name of the property to be fetched
// callback receives the result or nothing if none
// nothing logged, errors return nothing, so use with caution
function propertyFetch (pgClient, propertyName, callback) {
    // sanity check property, given nothing return same
    if (propertyName === undefined || propertyName === '' ) {
        callback();
    }
    if (propertyName != 'header-text' && propertyName != 'header-json') {
        let queryString = 'SELECT value FROM property WHERE name=\'' + propertyName + '\';';
        pgClient.query(queryString, function (err, result) {
            if (err) {
                callback();            
            }
            callback(result.rows[0].value);
        });
    }
    if (propertyName == 'header-text') {
        let packageObject = require('../package');
        let resultString = '{'
            + '\'' + 'name' + '\': \'' + packageObject.name + '\'' + ', '
            + '\'' + 'version' + '\': \'' + packageObject.version + '\'' + ', '
            + '\'' + 'description' + '\': \'' + packageObject.description + '\'' + '}';
        callback(resultString);
    }
    else if (propertyName == 'header-json') {
        let packageObject = require('../package');
        let resultObject = {
            'name': packageObject.name,
            'version': packageObject.version,
            'description': packageObject.description
        };
        callback(resultObject);
    }
}

var property = {};  // permit object to be exported by module
property.get = propertyFetch;
module.exports = property;

