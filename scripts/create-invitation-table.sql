-- invite store records for authentication and authorization
-- 
-- id is true key for database only never exposed even internally
-- ref is the uuid for internal use
-- provider is sending invitation
-- target is receiving invitation
-- code is currently all verification
-- challenge, response, salt not currently used
-- created
-- used
-- cancelled
DROP TABLE "invitation";
CREATE TABLE "invitation" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    provider UUID,
    target UUID,
    code TEXT,
    challenge TEXT,
    response TEXT,
    salt TEXT,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    used TIMESTAMP WITH TIME ZONE,
    cancelled TIMESTAMP WITH TIME ZONE
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE invitation TO tender;
GRANT USAGE, SELECT ON SEQUENCE invitation_id_seq TO tender;
-- seed with invitation for root owner account
INSERT INTO invitation (ref, provider, target, code) VALUES ('bd9885fa-1d1a-449d-93c8-fd252c47d93f', '44bccb91-4246-4756-88f0-15c19bdee903', '44bccb91-4246-4756-88f0-15c19bdee903', 'completelyoperational');

