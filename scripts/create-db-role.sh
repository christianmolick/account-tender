# all interactions with postgres are run as user postgres
# drop user to be sure of clean creation
sudo -u postgres dropuser tender
# role creation with options:
# D - no create dbs, R - no create roles, S - not superuser, P - prompt for password
# by default tender role uses password tdr
sudo -u postgres createuser -D -R -S -P tender
