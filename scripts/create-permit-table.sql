-- permits are the basic unit of authorization
-- upon authentication a permit is issued
-- permits may be revoked at any time for a number of reasons
-- such as duration limit or change to role or status
-- id is internal only, ref is a direct reference
-- account is the owner, issued is time of permit creation
-- host and agent are the point of access
-- role and status determine permission for actions

-- first drop table to clear any old data
DROP TABLE "permit";
-- actually create the table with these columns
CREATE TABLE "permit" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    account UUID,
    issued TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    host TEXT,
    agent TEXT,
    role TEXT,
    status TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE permit TO tender;
GRANT USAGE, SELECT ON SEQUENCE permit_id_seq TO tender;
-- no seed values 
