-- steps are the basic unit of sequencing
-- may represent challenges expecting response
-- verification codes to be returned

-- id is raw index for database only
-- ref is uuid for internal references
-- account refers to the owning account
-- host and agent store connection data
-- start and finish are timestamps
-- challenge is an index to challenges
-- code stores generate codes for verification steps

-- first drop table to clear it out
DROP TABLE "step";
-- actually create the table with these columns
CREATE TABLE "step" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    account UUID,
    host TEXT,
    agent TEXT,
    start TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    finish TIMESTAMP WITH TIME ZONE,
    challenge INTEGER,
    code TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE step TO tender;
GRANT USAGE, SELECT ON SEQUENCE step_id_seq TO tender;
-- no seed values 
