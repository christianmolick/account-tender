-- properties control account service operation
-- they are key value pairs of utf8 strings
-- with optional public flag and specified language
-- language would better be a short array of characters
-- units currently attached to name

-- first drop table to clear it out
DROP TABLE "property";
-- actually create the table with these columns
CREATE TABLE "property" (
    id SERIAL PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    language TEXT,
    public BOOLEAN,
    value TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE property TO tender;
GRANT USAGE, SELECT ON SEQUENCE property_id_seq TO tender;
-- currently no seed values
