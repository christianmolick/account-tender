# all interactions with postgres are as postgres user
# first drop database to be sure of clean creation
sudo -u postgres dropdb account
# create database with tender role and utf-8 encoding
sudo -u postgres createdb -O tender --encoding=utf8 account "account service provider"
# grant tender role permission to create tables
# no -W to force password as no longer considered best practice
sudo -u postgres psql -d account -e -f scripts/grant-db-role-privs.sql
