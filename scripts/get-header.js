// getheader gets the name, version, description header

let clientSource = require('node-rest-client').Client;

function getProperty(url, args, callback) {
    var client = new clientSource();
    var responseData = {};
    client.get(url, args, function(data, response) {
        if (response.statusCode === 404) {
            callback();
        } else {
            responseData = data;
            callback(responseData);
        }
    });
}

let getHeaderArgs = {
    data: {},
    headers: {'Content-Type': 'application/json'}
};

// host and port should be input, but hard code them to start
//getProperty('http://localhost:51112/property/header-text/', getHeaderArgs, function(result) {
getProperty('http://50.116.25.196:51112/property/header-text/', getHeaderArgs, function(result) {
    console.log('result: %s', JSON.stringify(result));
    console.log('as string: %s', result.toString('utf-8'));
    //.stringify(result));
});

