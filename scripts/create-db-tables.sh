# create service account database tables
# sudo -u postgres to run psql as postgres user
# -W to force password prompt no longer best practice
# -U no longer used?
# -d account indicates database
# -e to dump all sql
# -f to load from indicated file
sudo -u postgres psql -d account -e -f scripts/create-property-table.sql
sudo -u postgres psql -d account -e -f scripts/create-account-table.sql
sudo -u postgres psql -d account -e -f scripts/create-invitation-table.sql
sudo -u postgres psql -d account -e -f scripts/create-attribute-table.sql
sudo -u postgres psql -d account -e -f scripts/create-persona-table.sql
sudo -u postgres psql -d account -e -f scripts/create-step-table.sql
sudo -u postgres psql -d account -e -f scripts/create-permit-table.sql
