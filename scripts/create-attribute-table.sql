-- attribute table
-- used for storing account information
-- id is internal only, ref is used for internal direct identification
-- account is owner
-- type is name, contact, address, pronoun, or note
-- value and qualifier are the data
DROP TABLE "attribute";
CREATE TABLE "attribute" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    account UUID,
    type TEXT,
    value TEXT,
    qualifier TEXT
);
-- give full access to admin user/role
GRANT ALL PRIVILEGES ON TABLE attribute TO tender;
GRANT USAGE, SELECT ON SEQUENCE attribute_id_seq TO tender;
