-- accounts store records for authentication and authorization
-- as well as personal information
-- accessed through personas depending on contexts
-- 
-- id is true key for database only never exposed even internally
-- ref is the uuid for internal use
-- short_name used for identification and authorization
-- full_name for reference and use in direct communication
-- profile information about account holder and use
-- role controls authorization, currently provider or client
-- status for notes about handling
--     particularly critical operations notes like being banned
-- address location of client
-- contact methods and preferences for contact and notes of use
-- challenge, response, and salt are credentials
-- created is account creation moment
-- viewed is the time of last access
-- modified is the time of last change
DROP TABLE "account";
CREATE TABLE "account" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    short_name TEXT,
    full_name TEXT,
    profile TEXT,
    role TEXT,
    status TEXT,
    address TEXT,
    contact TEXT,
    challenge TEXT,
    response TEXT,
    salt TEXT,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE,
    modified TIMESTAMP WITH TIME ZONE
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE account TO tender;
GRANT USAGE, SELECT ON SEQUENCE account_id_seq TO tender;
-- seed with root provider shell account
-- uuid from https://www.uuidtools.com/v4
INSERT INTO account (ref, short_name, role, status) VALUES ('44bccb91-4246-4756-88f0-15c19bdee903', 'owner', 'provider', 'new');


