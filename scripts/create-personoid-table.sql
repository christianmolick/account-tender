-- personoid stores persona data fields
-- each personoid represents an attribute activation
-- 
-- id is true key for database only never exposed even internally
-- ref is the uuid direct reference for internal use
-- created is account creation moment
-- viewed is the time of last access
-- modified is the time of last change
-- account is the uuid reference for the source account
-- persona is the name of the persona
-- attribute is name, contact, address, pronoun, notes
-- value is the text of activated attribute
-- qualifier of activated attribute
DROP TABLE "personoid";
CREATE TABLE "personoid" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE,
    modified TIMESTAMP WITH TIME ZONE,
    account UUID,
    persona TEXT,
    attribute TEXT,
    value TEXT,
    qualifier TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE personoid TO tender;
GRANT USAGE, SELECT ON SEQUENCE personoid_id_seq TO tender;
