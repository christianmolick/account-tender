-- persona stores a selection of account field records
-- to be used to limit account information access by role
-- 
-- id is true key for database only never exposed even internally
-- ref is the uuid for internal use
-- created is account creation moment
-- viewed is the time of last access
-- modified is the time of last change
-- account is the uuid reference for the source account
-- persona is the name of the account persona
-- attribute is the name of the selected attribute
-- value is for the selected attribute
-- qualifier again defines the selected account attribute
DROP TABLE "persona";
CREATE TABLE "persona" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE,
    modified TIMESTAMP WITH TIME ZONE,
    account UUID,
    persona TEXT,
    attribute TEXT,
    value TEXT,
    qualifier TEXT   
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE persona TO tender;
GRANT USAGE, SELECT ON SEQUENCE persona_id_seq TO tender;
