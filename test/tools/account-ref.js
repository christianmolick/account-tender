// account-ref.js
// uses the database directly
// to look up account references by given attributes
// input is a list of attribute name and value pairs
// output is to console.log, often through JSON.stringify
// !!! NOT FINISHED OR WORKING

// internal modules
let config = require('../../app/config');  // database access string
// !!! BASE ONLY FOR DEFAULT VALUES AND GET AND POST ROUTINES, PROBABLY DROP
let base = require('../support/base');

// !!! DEBUG OUTPUT BECAUSE MYSTERY
console.log('db access from config = %s', config.dbAccessString);
console.log('test base account name = %s', base.testAccountName);

// external modules
// !!! ONLY NEEDED FOR INTERACTION, PROBABLY DROP
let readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});
let pg = require('pg');
let pgClient = new pg.Client(config.dbAccessString);
pgClient.connect(function (err) {
    if (err) {
        console.log('could not connect to database: %s', err);
    }
    //console.log('pg connection: %s', JSON.stringify(pgClient));
});
//console.log('pg connection: %s', JSON.stringify(pgClient));

// prepare to construct query using parameters
let matchQuery = 'SELECT ref,account FROM attribute WHERE ';
var glue = '';

// command line parameters
// process.argv has command line args with 0=node, 1=command, 2+=parms
for (var inputIndex = 2; inputIndex < process.argv.length; inputIndex = inputIndex + 2) {
    console.log('index = %d, type = %s, value = %s', inputIndex, process.argv[inputIndex], process.argv[inputIndex + 1]);
    
    // renaming for convenience
    let type = process.argv[inputIndex];
    let value = process.argv[inputIndex + 1];

    // !!! THIS QUERY GENERATION DOES NOT WORK
    // EXAMPLE: {"level":30,"time":1638729905590,"pid":13390,"hostname":"awayunit","msg":"attribute match query: SELECT ref,account FROM attribute WHERE (type='name' AND value='John Smith') AND (type='contact' AND value='test@invalid.org') ;"}
    // MAYBE LIKE THIS?
// select id, name, father_name
//  from employee
//  where (name, father_name) in (
//     select name, father_name
//       from employee
//       group by name, father_name
//       having count(*) > 1
//     )
//  order by father_name, name

    if (type === 'name') {
        matchQuery = matchQuery + '(type=\'name\' AND value=\'' + value + '\') ';
        glue = 'AND ';
    }
    else if (type === 'contact') {
        matchQuery = matchQuery + glue + '(type=\'contact\' AND value=\'' + value + '\') ';
        glue = 'AND ';
    }
    else if (type === 'address') {
        matchQuery = matchQuery + glue + '(type=\'address\' AND value=\'' + value + '\') ';
        glue = 'AND ';
    }
    else if (type === 'pronoun') {
        matchQuery = matchQuery + glue + ' (type=\'pronoun\' AND value=\'' + value + '\') ';
        glue = 'AND ';
    }
}
// !!! INDENTINGE
    // unrecognized types are skipped, values silently dropped
    matchQuery = matchQuery + ';';
    console.log('attribute match query: %s', matchQuery);
    
    let testQuery = 'SELECT * FROM attribute ; ';
    console.log('test query start: %s', testQuery);
    pgClient.query(testQuery, function(err, testResult) {
        if (err) {
            console.log('test query error: %s', err);
        }
        console.log('test query raw result: %s', JSON.stringify(testResult));
    });
    console.log('test query complete');
    
    console.log('query start');
    // !!! SKIP ACTUAL QUERY UNTIL STRING LOOKS RIGHT
    pgClient.query(matchQuery, function (err, matchResult) {
         if (err) {
            console.log('name attribute match query error %s', err);
            return;
        }
        console.log('ATTRIBUTE MATCH RAW RESULT: %s', JSON.stringify(matchResult));
    //    console.log('attribute match count: %d', matchResult.rowCount);
    //    if (matchResult.rowCount === 0) {
    //        // no name matches, return fail
    //        callback({result: 'failed', reason: 'not-found', count: 0});
    //        return;
    //    }
    //    else if (matchResult.rowCount === 1) {
    //        // one single name match, return that
    //        callback({result: 'okay', reason: 'account-found', count: 1, account: matchResult.rows[0].account});
    //        return;
    //    }
    //    else {
    //        // multiple matches, need more search factors
    //        callback ({result: 'failed', reason: 'multiple-matches', count: matchResult.rowCount});
    //        return;
    //    }
    });
    console.log('query complete');
    

// !!! NOT CLEAR WHY EXPLICIT PROCESS EXIT CALL IS REQUIRED
console.log('account reference lookup script complete');
//process._getActiveHandles();
//process._getActiveRequests();
process.exit();

