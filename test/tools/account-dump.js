// account-dump.js
// uses the service interface
// to look up and dump account information
// including attributes and personas

// external modules
let readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

// internal modules
let config = require('../../app/config');
let base = require('../support/base');

// command line parameters
// process.argv has command line args with 0=node, 1=command (this), 2+=parms
// with parameter being a string account reference

//console.log('command line arguments array: %s', JSON.stringify(process.argv));
let accountName = process.argv[2];
// verify command line parameters and substitute default values as appropriate
console.log('dumping data for account with name = %s', accountName);

// look up account from name
// separate ref fetch not needed because challenge fetch takes account name/data
// leaving this in for now
let refArgs = {
    data: {
        name: accountName
        },
    headers: {'Content-Type': 'application/json'}
};
// !!! really need to check first or fail better as eror result is a mess
base.get('http://localhost:' + config.servicePort + '/account/references', refArgs, function(refData) {
    // !!! need to fail gracefully if account reference fails
    // data.ref is account uuid
    console.log('account reference = %s', refData.account);
    // get challenge for sign in to account
    let getChallengeArgs = {
        data: {
            name: accountName
        },
        headers: {'Content-Type': 'application/json'}
    };
    base.get('http://localhost:' + config.servicePort + '/account/challenge', getChallengeArgs, function(challengeData) {
        // result.challenge should be the challenge
        console.log('account challenge: %s', challengeData.challenge);
        // get challenge response and return it
        readline.question('enter response to challenge: ', function(responseInput) {
            console.log('response received = %s', responseInput);
            readline.close();
            let postResponseArgs = {
                data: {
                    account: refData.account,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(responseData) {
                console.log('test permit = %s', responseData.permit);
                // !!! SHOULD EXPECT MULTIPLE CHALLENGES
                // account read, now inspect attributes and personas
                let inspectArgs = {
                    data: {
                        account: refData.account,
                        permit: responseData.permit
                    },
                    headers: {'Content-Type': 'application/json'}
                };
                base.get('http://localhost:' + config.servicePort + '/account/inspect', inspectArgs, function(inspectData) {
                    console.log('account inspect data: %s', JSON.stringify(inspectData));
                    // list personas
                    console.log('persona list args ...');
                    let personasListArgs = {
                        data: {
                            account: refData.account,
                            permit: responseData.permit
                        },
                        headers: {'Content-Type': 'application/json'}
                    };
                    console.log('persona list args: %s', JSON.stringify(personasListArgs));
                    base.get('http://localhost:' + config.servicePort + '/persona/list', personasListArgs, function(personasListData) {
                        console.log('personas list fetched: %s', JSON.stringify(personasListData));
                        // dump all personas
                        let personaRecordsArgs = {
                            data: {
                                account: refData.account,
                                permit: responseData.permit
                            },
                            headers: {'Content-Type': 'application/json'}
                        };
                        console.log('persona records args: %s', JSON.stringify(personaRecordsArgs));
                        base.get('http://localhost:' + config.servicePort + '/persona/records', personaRecordsArgs, function(personaRecordsData) {
                            console.log('persona records fetch result: %s', JSON.stringify(personaRecordsData));

                        //for (var personaIndex = 0; personaIndex < personasListData.personas.length; personaIndex++) {
                        //    console.log('inspecting persona #%d = %s', personaIndex, personasListData.personas[personaIndex]);
                        //    let personaInspectArgs = {
                        //        data: {
                        //            account: refData.account,
                        //            permit: responseData.permit,
                        //            persona: personasListData.personas[personaIndex]
                        //        },
                        //        headers: {'Content-Type': 'application/json'}
                        //    };
                            // console.log('fetching persona #%d = %s', personaIndex, personasListData.personas[personaIndex]);
                        //    base.get('http://localhost:' + config.servicePort + '/persona/inspect', personaInspectArgs, function(personaInspectData) {
                        //        console.log('persona data: %s', JSON.stringify(personaInspectData));
                        //    });  // persona inspect block

                            // work done, cancel permit to sign out
                            let permitCancelArgs = {
                                data: {
                                    permit: responseData.permit
                                },
                                headers: {'Content-Type': 'application/json'}
                            };
                            base.post('http://localhost:' + config.servicePort + '/permit/cancel', permitCancelArgs, function(cancelData) {
                                console.log('sign out permit cancel response: %s', JSON.stringify(cancelData));
                            });  // end cancel permit block
                            
                        });  // persona records fetch block
                    //    }  // personas list iteration block
                    }); //  end list personas block
                });  // account inspect block
            });  // end post response block
        });  // end read response input block
    });  // end get challenge block
});  // end get reference block
