// account-deref.js
// experimental code for finding an account given attributes

// required modules and preparations
let { v4: uuidv4 } = require('uuid');
let pg = require('pg');
let pgClient = new pg.Client('postgres://tender:tdr@localhost:5432/account');
pgClient.connect(function (err) {
    if (err) {
        console.log('could not connect to database: %s', err);
    }
    console.log('pg connection parameters: %s', JSON.stringify(pgClient.connectionParameters));

// create accounts with attributes for testing
let id1 = '11111111-1111-1111-1111-111111111111';
//let id1 = uuidv4();
let acc1 = 'INSERT INTO account (ref) VALUES (\'' + id1 + '\');';
console.log('account creation query: %s', acc1);
pgClient.query(acc1, function (err, result) {
    if (err) {
        console.log('account 1 creation error: %s', err);
    }
    console.log('account 1 creation rows: %d', result.rowCount);

let attr1 = 'INSERT INTO attribute (account,type,value) VALUES ';
attr1 = attr1 + '(\'' + id1 + '\',\'name\',\'jsmith\'),';
attr1 = attr1 + '(\'' + id1 + '\',\'name\',\'John Smith\'),';
attr1 = attr1 + '(\'' + id1 + '\',\'contact\',\'jsmith@usa.gov\');';
console.log('account attribute query: %s', attr1);
pgClient.query(attr1, function (err, result) {
    if (err) {
        console.log('account 1 attributes creation error: %s', err);
    }
    console.log('account 1 attributes creation row count: %d', result.rowCount);

let id2 = '22222222-2222-2222-2222-222222222222';
//let id2 = uuidv4();
let acc2 = 'INSERT INTO account (ref) VALUES (\'' + id2 + '\');';
console.log('account creation query: %s', acc2);
pgClient.query(acc2, function (err, result) {
    if (err) {
        console.log('account 2 creation error: %s', err);
    }
    console.log('account 2 creation rows: %d', result.rowCount);

let attr2 = 'INSERT INTO attribute (account,type,value) VALUES ';
attr2 = attr2 + '(\'' + id2 + '\',\'name\',\'jsmith\'),';
attr2 = attr2 + '(\'' + id2 + '\',\'name\',\'John Smith\'),';
attr2 = attr2 + '(\'' + id2 + '\',\'contact\',\'111-111-1111\');';
console.log('account attribute query: %s', attr2);
pgClient.query(attr2, function (err, result) {
    if (err) {
        console.log('account 2 attributes creation error: %s', err);
    }
    console.log('account 2 attributes creation row count: %d', result.rowCount);

let id3 = '33333333-3333-3333-3333-333333333333';
//let id3 = uuidv4();
let acc3 = 'INSERT INTO account (ref) VALUES (\'' + id3 + '\');';
console.log('account creation query: %s', acc3);
pgClient.query(acc3, function (err, result) {
    if (err) {
        console.log('account 3 creation error: %s', err);
    }
    console.log('account 3 creation rows: %d', result.rowCount);

let attr3 = 'INSERT INTO attribute (account,type,value) VALUES ';
attr3 = attr3 + '(\'' + id3 + '\',\'name\',\'jsmith\'),';
attr3 = attr3 + '(\'' + id3 + '\',\'name\',\'Johnathan Grumpnoodle Smith\'),';
attr3 = attr3 + '(\'' + id3 + '\',\'contact\',\'222-222-2222\');';
console.log('account attribute query: %s', attr3);
pgClient.query(attr3, function (err, result) {
    if (err) {
        console.log('account 3 attributes creation error: %s', err);
    }
    console.log('account 3 attributes creation row count: %d', result.rowCount);

// test disambiguated queries of accounts using attribute values
// 1. full name and email
    
// MAYBE LIKE THIS?
// select id, name, father_name
//  from employee
//  where (name, father_name) in (
//     select name, father_name
//       from employee
//       group by name, father_name
//       having count(*) > 1
//     )
// notes: WITH query SELECT list FROM table sorting
// FROM LATERAL enables refs to colums from preceeding FROM
// WHERE boolean  /  GROUP BY column[,column]  /  HAVING boolean

//select (account) from attribute where (type='name' and value='John Smith') or (type='contact' and value='jsmith@usa.gov') group by account;

//select account from attribute where account in (select account from attribute group by account)

// account=# select (account,type,value) from attribute where (type='name' and value='John Smith') or (type='contact' and value='jsmith@usa.gov');
//                              row                              
//---------------------------------------------------------------
// (11111111-1111-1111-1111-111111111111,name,"John Smith")
// (11111111-1111-1111-1111-111111111111,contact,jsmith@usa.gov)
// (22222222-2222-2222-2222-222222222222,name,"John Smith")
//(3 rows)
    
// account=# select (account,type,value) from attribute where (type='name' and value='John Smith') or (type='contact' and value='jsmith@usa.gov');

// select * from attribute where (type='name' and value='John Smith') 

//select (account,type,value) from attribute where (account,type,value) in (select (account,type,value) from attribute where (type='name' and value='John Smith') or (type='contact' and value='jsmith@usa.gov'));

//select (account) from attribute where (account) in (select (account,type,value) from attribute where (type='name' and value='John Smith') or (type='contact' and value='jsmith@usa.gov'));
    
    
//select account from attribute where (type='name' and value='John Smith');
//select account from attribute where (type='contact' and value='jsmith@usa.gov');

//let matchQuery = 'SELECT account FROM attribute WHERE account IN (';
//matchQuery = matchQuery + 'SELECT account FROM attribute WHERE ';
//matchQuery = matchQuery + '(type=\'name\' AND value=\'John Smith\') ';
//matchQuery = matchQuery + ' OR (type=\'contact\' AND value=\'jsmith@usa.gov\') ';
//matchQuery = matchQuery + ');';

let matchQuery = 'SELECT (account,type,value) FROM attribute WHERE ';
//matchQuery = matchQuery + 'SELECT account FROM attribute WHERE ';
matchQuery = matchQuery + '(type=\'name\' AND value=\'John Smith\') ';
matchQuery = matchQuery + ' OR (type=\'contact\' AND value=\'jsmith@usa.gov\') ';
matchQuery = matchQuery + ';';
//matchQuery = matchQuery + 'GROUP BY account HAVING count(*) > 1;';

console.log('attribute match query: %s', matchQuery);
pgClient.query(matchQuery, function (err, matchResult) {
    if (err) {
        console.log('name attribute match query error %s', err);
    }
    //console.log('ATTRIBUTE MATCH RAW RESULT: %s', JSON.stringify(matchResult));
    console.log('attribute match count: %d', matchResult.rowCount);
    console.log('attribute match rows: %s', JSON.stringify(matchResult.rows));


// 2. short name and phone
// 3. extra long name and email
    
// !!! EXIT WITHOUT DELETING TO TRY COMMAND LINE HOEKERY
process.exit();

// delete all of the test accounts and attributes
let del1 = 'DELETE FROM account WHERE ref=\'' + id1 + '\'; ';
del1 = del1 + 'DELETE FROM attribute WHERE account = \'' + id1 + '\';';
pgClient.query(del1, function (err, result) {
    if (err) {
        console.log('account 1 deletion error: %s', err);
    }
    console.log('account 1 deletion rows %d, more rows %d', result[0].rowCount, result[1].rowCount);

let del2 = 'DELETE FROM account WHERE ref=\'' + id2 + '\'; ';
del2 = del2 + 'DELETE FROM attribute WHERE account = \'' + id2 + '\';';
pgClient.query(del2, function (err, result) {
    if (err) {
        console.log('account 2 deletion error: %s', err);
    }
    console.log('account 2 deletion rows %d, more rows %d', result[0].rowCount, result[1].rowCount);

let del3 = 'DELETE FROM account WHERE ref=\'' + id3 + '\'; ';
del3 = del3 + 'DELETE FROM attribute WHERE account = \'' + id3 + '\';';
pgClient.query(del3, function (err, result) {
    if (err) {
        console.log('account 3 deletion error: %s', err);
    }
    console.log('account 3 deletion rows %d, more rows %d', result[0].rowCount, result[1].rowCount);

// kill me!!1
process.exit();

});  // end delete 3
});  // end delete 2
});  // end delete 1
});  // end test 1
});  // end create attributes 3
});  // end create account 3
});  // end create attributes 2
});  // end create account 2
});  // end create attributes 1
});  // end create account 1
});  // end connect


