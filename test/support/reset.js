// reset.js: Test account credential reset functionality.
// Currently only a sketch as reset functionality and interfaces are being implemented.

// !!! reset functionality incomplete and unusable
// !!! REFACTOR ASSERTIONS TO NOT USE CHAI

// initiate reset
//describe('initiate account reset', function () {
//    it('receives confirmation of code sent using contact info', function(done) {
//        let resetInitUrl = 'http://localhost:' + config.servicePort
//            + '/account/reset';
//        let resetInitArgs = {
//            data: {
//                name: mochaTestAccount
//            },
//            headers: {'Content-Type': 'application/json'}
//        };
//        postProperty(resetInitUrl, resetInitArgs, 
//        function(data) {
//            data.result.should.equal('done');
//            done();
//        });
//    });
//});

// validate reset by return of reset code
// at this time the code is not sent or checked
// until full implementation this only exercises the code path
//describe('return reset code', function () {
//    it('receives confirmation correct code received', function(done) {
//        let receivedCode = 0;
//        let resetCodeUrl = 'http://localhost:' + config.servicePort 
//             + '/account/reset';
//        let resetCodeArgs = {
//            data: {
//                name: mochaTestAccount,
//                code: receivedCode
//            },
//            headers: {'Content-Type': 'application/json'}
//        };
//        postProperty(resetCodeUrl, resetCodeArgs, 
//            function(data) {
//                data.result.should.equal('okay');
//                done();
//            } 
//        );
//    });
//});

// complete reset with credentials receive step
//describe('send reset credentials', function () {
//    it('receives confirmation of credentials reset for account', function(done) {
//        let resetCredUrl = 'http://localhost:' + config.servicePort 
//              + '/account/reset';
//        let resetCredArgs = {
//            data: {
//                name: mochaTestAccount,
//                challenge1: 'x',
//                response1: 'y'
//            },
//            headers: {'Content-Type': 'application/json'}
//        };
//        postProperty(resetCredUrl, resetCredArgs, 
//            function(data) {
//                data.result.should.equal('okay');
//                done();
//            }
//        );
//    });
//});

// confirm ability to sign in to reset account: get challenge
//describe('get reset account challenge', function() {
//    it('returned account challenge', function(done) {
//        let getChallengeUrl = 'http://localhost:' + config.servicePort 
//              + '/account/challenge';
//        let getChallengeArgs = {
//            data: {
//                name: mochaTestAccount
//            },
//            headers: {'Content-Type': 'application/json'}
//        };
//        getProperty(getChallengeUrl, getChallengeArgs, 
//            function(data) {
//                data.should.deep.equal({
//                        result: 'done', 
//                        reason: 'account-read', 
//                        challenge: 'hey'});
//                done();                    
//            }
//        );
//    });
//});

// confirm ability to sign in to reset account: post response
//describe('post reset account challenge response', function() {
//    it('accepted correct challenge response', function(done) {
//        let postResponseUrl = 'http://localhost:' + config.servicePort 
//              + '/account/response';
//        let postResponseArgs = {
//            data: {
//                name: mochaTestAccount,
//                response: saltedHashedResponse
//            },
//            headers: {'Content-Type': 'application/json'}
//        };
//        postProperty(postResponseUrl, postResponseArgs, 
//            function(data) {
//                issuedPermit = data.permit;
//                data.result.should.equal('okay');
//                data.reason.should.equal('response-valid');
//                done();
//            }
//        );
//    });
//});
