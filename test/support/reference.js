// reference.js: Test potentially ambiguous account references.
// This consists of: request two accounts in conflict
// then try ambigous and disambiguated queries.
// These test should be near the start 
// as references are required for most account actions inluding signing in.
// Aside: This is "reference" singualar to avoid most pluralization
// but some internal code refers in plural to "references".

// require modules for service configuration and base testing
let assert = require('assert');
let config = require('../../app/config');
let base = require('./base');

// define object for sharing test routine
var reference = {};

// test values specific to this check of referencing conflicts
let testFullName1 = 'John Smith';
let testFullName2 = 'Johnathan Hercule Smith';

// internal storage of test account references
var testAccount1;
var testAccount2;
var testAccount3;

// test permit used for sign in and deletion of each of three accounts
var testPermit;

// testReferences function is exported and executes all reference tests
function testReferences() {
    // *** first request three test accounts with various conflicts
    // request first account
    describe('reference: request account 1', function() {
        it('created requested account', function(done) {
            let requestAccountArgs = {
                data: {
                    credentials: [base.testChallenge, base.testResponse],
                    attributes: [
                        'name', base.testAccountName, 'short',
                        'name', testFullName1, 'full',
                        'contact', base.testEmail, 'primary email'
                    ]
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/request', requestAccountArgs, function(data) {
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-created');
                testAccount1 = data.ref;
                done();
            });
        });
    });

    // request second account with short name conflict
    describe('reference: request account 2', function() {
        it('created requested account', function(done) {
            let requestAccountArgs = {
                data: {
                    credentials: [base.testChallenge, base.testResponse],
                    attributes: [
                        'name', base.testAccountName, 'short',
                        'name', testFullName1, 'full',
                        'contact', base.testPhone, 'primary phone'
                    ]
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/request', requestAccountArgs, function(data) {
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-created');
                testAccount2 = data.ref;
                done();
            });
        });
    });

    // request third account with full name conflict
    describe('reference: request account 3', function() {
        it('created requested account', function(done) {
            let requestAccountArgs = {
                data: {
                    credentials: [base.testChallenge, base.testResponse],
                    attributes: [
                        'name', base.testAccountName, 'short',
                        'name', testFullName2, 'full',
                        'contact', base.testEmail, 'primary email'
                    ]
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/request', requestAccountArgs, function(data) {
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-created');
                testAccount3 = data.ref;
                done();
            });
        });
    });

    // *** second, perform conflicted and correctly specified reference lookups
    // * first conflicted lookups that should fail
    describe('reference: fetch account references with short name conflict', function() {
        it('returns failure', function(done) {
            let referencesArgs = {
                data: {
                    name: base.testAccountName
                 },
                 headers: {'Content-Type': 'application/json'}
             };
             base.get('http://localhost:' + config.servicePort + '/account/references', referencesArgs, function(data) {
                assert.ok(data.result === 'failed');
                assert.ok(data.reason === 'multiple-matches');
                done();
            });
        });
    });

    // look up account references: full name conflict
    describe('reference: fetch account references with full name conflict', function() {
        it('returns failure', function(done) {
            let referencesArgs = {
                data: {
                    name: testFullName1
                 },
                 headers: {'Content-Type': 'application/json'}
             };
             base.get('http://localhost:' + config.servicePort + '/account/references', referencesArgs, function(data) {
                assert.ok(data.result === 'failed');
                assert.ok(data.reason === 'multiple-matches');
                done();
            });
        });
    });

    // look up account references: email conflict
    describe('reference: fetch account references with email conflict', function() {
        it('returns failure', function(done) {
            let referencesArgs = {
                data: {
                    contact: base.testEmail
                 },
                 headers: {'Content-Type': 'application/json'}
             };
             base.get('http://localhost:' + config.servicePort + '/account/references', referencesArgs, function(data) {
                assert.ok(data.result === 'failed');
                assert.ok(data.reason === 'multiple-matches');
                done();
            });
        });
    });

    // * now correct reference lookups that should succeed
    // look up account references: full name and email
    describe('reference: fetch account references by name and email', function() {
        it('returns reference', function(done) {
            let referencesArgs = {
                data: {
                    name: testFullName1,
                    contact: base.testEmail,
                 },
                 headers: {'Content-Type': 'application/json'}
             };
             base.get('http://localhost:' + config.servicePort + '/account/references', referencesArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-found');
                assert.ok(data.account === testAccount1);
                done();
            });
        });
    });

    // look up account references: short name and phone contact
    describe('reference: fetch account references by name and phone', function() {
        it('returns reference', function(done) {
            let referencesArgs = {
                data: {
                    name: base.testAccountName,
                    contact: base.testPhone
                 },
                 headers: {'Content-Type': 'application/json'}
             };
             base.get('http://localhost:' + config.servicePort + '/account/references', referencesArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-found');
                assert.ok(data.account === testAccount2);
                done();
            });
        });
    });

    // look up account references: full name and email contact
    describe('reference: fetch account references by full name and email', function() {
        it('returns reference', function(done) {
            let referencesArgs = {
                data: {
                    name: testFullName2,
                    contact: base.testEmail
                 },
                 headers: {'Content-Type': 'application/json'}
             };
             base.get('http://localhost:' + config.servicePort + '/account/references', referencesArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-found');
                assert.ok(data.account === testAccount3);
                done();
            });
        });
    });
    
    // *** tests completed: now sign into and delete accounts
    // * test account 1
    // sign in: get challenge
    describe('reference: get account challenge', function() {
        it('returned account challenge', function(done) {
            // note get challenge can use name text or account reference directly
            let getChallengeArgs = {
                data: {
                    name: testFullName1,
                    contact: base.testEmail
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/challenge', getChallengeArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-read');
                assert.ok(data.challenge === base.testChallenge);
                done();                    
            });
        });
    });

    // sign in: post response
    describe('reference: post account challenge response', function() {
        it('accepted correct challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    account: testAccount1,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(data) {
                testPermit = data.permit;
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'response-valid');
                done();
            });
        });
    });

    // delete: post deletion of the test account                                        
    describe('reference: post account deletion', function() {
        it('create deletion confirmation step', function(done) {
            let postDeleteArgs = {
                data: {
                    account: testAccount1,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', postDeleteArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'deletion-initiated');
                done();
            });
        });
    });

    // confirm deletion of the test account
    describe('reference: confirm account deletion', function() {
        it('successful account deletion', function(done) {
            let confirmDeleteArgs = {
                data: {
                    account: testAccount1,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', confirmDeleteArgs, function(data) {
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-deleted');
                done();
            });
        });
    });

    // * test account 2
    // sign in: get challenge
    describe('reference: get account challenge', function() {
        it('returned account challenge', function(done) {
            // note get challenge can use name text or account reference directly
            let getChallengeArgs = {
                data: {
                    name: base.testAccountName,
                    contact: base.testPhone
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/challenge', getChallengeArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-read');
                assert.ok(data.challenge === base.testChallenge);
                done();                    
            });
        });
    });

    // sign in: post response
    describe('reference: post account challenge response', function() {
        it('accepted correct challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    account: testAccount2,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(data) {
                testPermit = data.permit;
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'response-valid');
                done();
            });
        });
    });
    
    // delete: post deletion of the test account                                        
    describe('reference: post account deletion', function() {
        it('create deletion confirmation step', function(done) {
            let postDeleteArgs = {
                data: {
                    account: testAccount2,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', postDeleteArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'deletion-initiated');
                done();
            });
        });
    });

    // confirm deletion of the test account
    describe('reference: confirm account deletion', function() {
        it('successful account deletion', function(done) {
            let confirmDeleteArgs = {
                data: {
                    account: testAccount2,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', confirmDeleteArgs, function(data) {
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-deleted');
                done();
            });
        });
    });

    // * test account 3
    // sign in: get challenge
    describe('reference: get account challenge', function() {
        it('returned account challenge', function(done) {
            // note get challenge can use name text or account reference directly
            let getChallengeArgs = {
                data: {
                    name: testFullName2,
                    contact: base.testEmail
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/challenge', getChallengeArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-read');
                assert.ok(data.challenge === base.testChallenge);
                done();                    
            });
        });
    });

    // sign in: post response
    describe('reference: post account challenge response', function() {
        it('accepted correct challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    account: testAccount3,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(data) {
                testPermit = data.permit;
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'response-valid');
                done();
            });
        });
    });

    // delete: post deletion of the test account                                        
    describe('reference: post account deletion', function() {
        it('create deletion confirmation step', function(done) {
            let postDeleteArgs = {
                data: {
                    account: testAccount3,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', postDeleteArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'deletion-initiated');
                done();
            });
        });
    });

    // confirm deletion of the test account
    describe('reference: confirm account deletion', function() {
        it('successful account deletion', function(done) {
            let confirmDeleteArgs = {
                data: {
                    account: testAccount3,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', confirmDeleteArgs, function(data) {
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-deleted');
                done();
            });
        });
    });

}
reference.test = testReferences;
module.exports = reference;

