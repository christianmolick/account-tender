// base.js
// test module defines basic terms and routines

// base object to later export
// base.js
// defines essential values and routines

var base = {};

// define values to use in tests
base.invalidRef = '11111111-1111-1111-1111-111111111111';
base.testAccountName = 'test account name';
base.testAccountFullName = 'Test Account Full Name';
base.alternativeFullName = 'Alternative Full Name';
base.testEmail = 'test@invalid.org';
base.alternativeEmail = 'alternative@invalid.org';
base.testPhone = '1-111-111-1111';
base.updatedProfile = 'updated profile';
base.testChallenge = 'hey';
base.testResponse = 'what';
// hrm
base.seedAccount = '44bccb91-4246-4756-88f0-15c19bdee903';
base.seedInvitation = 'bd9885fa-1d1a-449d-93c8-fd252c47d93f';
base.seedCode = 'completelyoperational';

// getProperty and postProperty use account tender interfaces
// with the node rest client module
let clientSource = require('node-rest-client').Client;

function getProperty(url, args, callback) {
    var client = new clientSource();
    var responseData = {};
    client.get(url, args, function(data, response) {
        if (response.statusCode === 404) {
            callback();
        } else {
            responseData = data;
            callback(responseData);
        }
    });
}

function postProperty(url, args, callback) {
    let client = new clientSource();
    var responseData = {};
    client.post(url, args, function(data) { 
        responseData = data;
        callback(responseData);
    });
}

// define methods and export module object
base.get = getProperty;
base.post = postProperty;
module.exports = base;
