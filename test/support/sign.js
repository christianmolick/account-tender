// sign.js: Test in detail various options for signing in to accounts
// including invalid sign ins that should fail.

// look up account references
describe('fetch account references by name', function() {
    it('returns reference, name, and full name', function(done) {
        let referencesArgs = {
            data: {
                name: testAccountName
             },
             headers: {'Content-Type': 'application/json'}
         };
         getProperty('http://localhost:' + config.servicePort + '/account/references', referencesArgs, function(data) {
            data.result.should.equal('okay');
            data.reason.should.equal('account-read');
            data.account.should.equal(testAccountRef);
            done();
         });
    });
});

// get challenge
describe('get account challenge', function() {
    it('returned account challenge', function(done) {
        let getChallengeArgs = {
            data: {
                name: testAccountName
             },
             headers: {'Content-Type': 'application/json'}
        };
        getProperty('http://localhost:' + config.servicePort + '/account/challenge', getChallengeArgs, function(data) {
            data.should.deep.equal({
                result: 'okay', 
                reason: 'account-read', 
                challenge: testChallenge});
            done();                    
        });
    });
});

// post response
describe('post account challenge response', function() {
    it('accepted correct challenge response', function(done) {
        let postResponseArgs = {
            data: {
                account: testAccountRef,
                response: testResponse
            },
            headers: {'Content-Type': 'application/json'}
        };
        postProperty('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(data) {
            testPermit = data.permit;
            data.result.should.equal('okay');
            data.reason.should.equal('response-valid');
            done();
        });
    });
});

// check sign in permit for basic read access
describe('check permit for basic read access', function() {
    it ('correctly returns status of permit for user and usage', function(done) {
        let checkArgs = {
            data: {
                account: testAccountRef,
                permit: testPermit,
                usage: 'read'
            },
            headers: {'Content-Type': 'application/json'}
        };
        getProperty('http://localhost:' + config.servicePort + '/permit/valid', checkArgs, function(data) {
            data.result.should.equal('okay');
            done();
        });
    });
});
