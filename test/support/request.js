// request.js: Test options for account requests
// including complex requests and invalid requests that should fail.
// This is intended to be more exhaustive than the basic cycle tests
// which are more of a reality check.
// 
// !!! this is a first sketch which is not currently used
//     the most functionality tested in cycle tests

// require modules for service configuration and base testing
let assert = require('assert');
let config = require('../../app/config');
let base = require('./base');

// define object for sharing test routine
var request = {};

// internal sorage of test account and permit references
var testAccountRef;
var testPermit;

// testRequest function is exported and executes all test
function testRequest() {
    // invalid account request?

    // request account without credentials
    describe('request: request account without credentials', function() {
        it('created requested account', function(done) {
            let requestAccountArgs = {
                data: {
                    attributes: [
                        'name', base.testAccountName, 'short',
                        'name', base.testAccountFullName, 'full',
                        'contact', base.testEmail, 'email'
                    ]
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/request', requestAccountArgs, function(data) {   
                testAccountRef = data.ref;
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-created');
                done();
            });
        });
    });
    
    // request account credentials
    describe('request account credentials', function() {
        it('successfully requests a set of authentication credentials', function(done) {
            let credArgs = {
                data: {
                    'account': testAccountRef,
                    challenge: base.testChallenge,
                    response: base.testResponse,
                    confirm: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/credential', credArgs, function(data) {
                assert.ok(data.result === 'done');
                done();
            });
        });
    });
    
    // request additional credentials
    describe('request more account credentials', function() {
        it('successfully requests second set of credentials', function(done) {
            let moreCredArgs = {
                data: {
                    'account': testAccountRef,
                    challenge: base.testChallenge + base.testChallenge,
                    response: base.testResponse + base.testResponse,
                    confirm: base.testResponse + base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/credential', moreCredArgs, function(data) {
                assert.ok(data.result === 'done');
                done();
            });
        });
    });
    
    // request account completion
    describe('request account completion', function() {
        it('successfully credentialed account', function(done) {
            let completeArgs = {
                data: {},
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/complete/' + testAccountRef, completeArgs, function(data) {
                assert.ok(data.result === 'done');
                done();
            });
        });
    });
    
    // test account credentialing
    describe('test account credentialing', function() {
        it('returns correct time of account credentialing', function(done) {
            let credArgs = {
                data: {},
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/credentialed/' + testAccountRef, credArgs, function(data) {
                assert.ok(data.result === 'okay');
                // test that the returned time makes sense? at least the format?
                done();
            });
        });
    });
    
    // request account with many options?

    // account request testing complete, now clean up the mess
    // sign in to account to get a permit for deletion
    // this adds testing of the multiple challenge sign in case

    // get challenge
    describe('request: get account challenge', function() {
        it('returned account challenge', function(done) {
            let getChallengeArgs = {
                data: {
                    name: base.testAccountName
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/challenge', getChallengeArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-read');
                assert.ok(data.challenge === base.testChallenge);
                done();                    
            });
        });
    });

    // post response
    describe('request: post account challenge response', function() {
        it('accepted correct challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    'account': testAccountRef,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(data) {
                assert.ok(data.result === 'okay');
                done();
            });
        });
    });

    // post additional response
    describe('request: post account challenge response', function() {
        it('accepted correct challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    'account': testAccountRef,
                    response: base.testResponse + base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(data) {
                testPermit = data.permit;
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'response-valid');
                done();
            });
        });
    });
    
    // delete test account

    // post deletion of the test account                                        
    describe('request: post account deletion', function() {
        it('create deletion confirmation step', function(done) {
            let postDeleteArgs = {
                data: {
                    'account': testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', postDeleteArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'deletion-initiated');
                done();
            });
        });
    });

    // confirm deletion of the test account
    describe('request: confirm account deletion', function() {
        it('successful account deletion', function(done) {
            let confirmDeleteArgs = {
                data: {
                    'account': testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', confirmDeleteArgs, function(data) {
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-deleted');
                done();
            });
        });
    });
}

request.test = testRequest;
module.exports = request;

