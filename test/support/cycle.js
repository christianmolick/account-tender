// cycle.js: Test initial basic account functionality.
// This consists of: request, get reference by name,
// sign in, inspect, sign out, and delete account.
// These checks should be done before any other tests that require accounts.

// require modules for service configuration and base testing
let assert = require('assert');
let config = require('../../app/config');
let base = require('./base');

// define object for sharing test routine
var cycle = {};

// internal storage of test account and permit references
var testAccountRef;
var testPermit;

// testCycle function is exported and executes all cycle tests
function testCycle() {

    // request account
    describe('cycle: request account', function() {
        it('created requested account', function(done) {
            // credentials: req.body.credentials - challenge and response pairs
            // attributes: req.body.attributes - type, value, qualifier triples
            let requestAccountArgs = {
                data: {
                    credentials: [base.testChallenge, base.testResponse],
                    attributes: [
                        'name', base.testAccountName, 'short',
                        'name', base.testAccountFullName, 'full',
                        'contact', base.testEmail, 'primary email'
                    ]
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/request', requestAccountArgs, function(data) {
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-created');
                testAccountRef = data.ref;
                done();
            });
        });
    });
    
    // look up account references
    describe('cycle: fetch account references by name', function() {
        it('returns reference, name, and full name', function(done) {
            let referencesArgs = {
                data: {
                    name: base.testAccountName
                 },
                 headers: {'Content-Type': 'application/json'}
             };
             base.get('http://localhost:' + config.servicePort + '/account/references', referencesArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-found');
                assert.ok(data.account === testAccountRef);
                done();
            });
        });
    });

    // sign in: get challenge
    describe('cycle: get account challenge', function() {
        it('returned account challenge', function(done) {
            // note get challenge can use name text or account reference directly
            let getChallengeArgs = {
                data: {
                    name: base.testAccountName
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/challenge', getChallengeArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-read');
                assert.ok(data.challenge === base.testChallenge);
                done();                    
            });
        });
    });

    // sign in: post response
    describe('cycle: post account challenge response', function() {
        it('accepted correct challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    account: testAccountRef,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(data) {
                testPermit = data.permit;
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'response-valid');
                done();
            });
        });
    });

    // basic permit check for read access
    describe('cycle: check permit for basic read access', function() {
        it ('correctly returns status of permit for user and usage', function(done) {
            let checkArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    usage: 'read'
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/permit/valid', checkArgs, function(data) {
                assert.ok(data.result === 'okay');
                done();
            });
        });
    });

    // inspect authenticated account 
    describe('cycle: inspect account', function() {
        it('correctly recalls account data', function(done) {
            let inspectArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/inspect', inspectArgs, function(data) {
                assert.ok(data.result === 'okay');
                done();
            });
        });
    });

    // delete: post deletion of the test account                                        
    describe('cycle: post account deletion', function() {
        it('create deletion confirmation step', function(done) {
            let postDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', postDeleteArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'deletion-initiated');
                done();
            });
        });
    });

    // confirm deletion of the test account
    describe('cycle: confirm account deletion', function() {
        it('successful account deletion', function(done) {
            let confirmDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', confirmDeleteArgs, function(data) {
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-deleted');
                done();
            });
        });
    });

}
cycle.test = testCycle;
module.exports = cycle;

