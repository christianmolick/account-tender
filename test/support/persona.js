// persona.js: Test persona related functionality
// consisting of persona request, inspect, update, toggle, and basic usage
// and also invalid calls which should fail.

// require modules for service configuration, base testing
// and arrange and complete to set up and clean up testing context
let config = require('../../app/config');
let base = require('./base');

// define object for sharing test routine
var persona = {};

// testPersonas function is exported and executes all tests
function testPersonas() {
    describe('persona: create, inspect ', function() {
        // first set up test context by making account and signing in

        // define variables to store account and sign in permit references
        var testAccountRef;
        var testPermit;
        // request account
        it('persona: request account', function(done) {
            let requestAccountArgs = {
                data: {
                    credentials: [base.testChallenge, base.testResponse],
                    attributes: [
                        'name', base.testAccountName, 'short',
                        'name', base.testAccountFullName, 'full',
                        'contact', base.testEmail, 'primary email'
                    ]
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/request', requestAccountArgs, function(data) {                
                testAccountRef = data.ref;
                data.result.should.equal('okay');
                data.reason.should.equal('account-created');
                done();
            });
        });  // it requests account block
        // request challenge for newly created test account
        it('persona: request challenge', function(done) {
            // get challenge
            let getChallengeArgs = {
                data: {
                    name: base.testAccountName
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/challenge', getChallengeArgs, function(data) {
                data.result.should.equal('okay');
                data.reason.should.equal('account-read');
                done();
            });
        });  // it get challenge block
        // post response to challenge to complete account sign in
        it('persona: post challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    account: testAccountRef,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(data) {
                testPermit = data.permit;
                data.result.should.equal('okay');
                data.reason.should.equal('response-valid');
                done();
            });
        });  // it posts challenge response and receives permit block

        // now test using the freshly made account and sign in permit
        var providerPersona;
        var clientPersona;
        // request provider persona
        it('persona: successfully creates provider persona', function (done) {
            let args = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    name: 'provider'
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/persona/request', args, function(data) {
                // expected result is okay instead of done because this is a request
                data.result.should.equal('okay');
                providerPersona = data.persona;
                done();
            });
        });

        // request client persona for alternate account
        // note: how best to differentiates client and provider personas unclear
        it('persona: successfully creates client persona', function (done) {
            let args = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    name: 'client'
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/persona/request', args, function(data) {
            // expected result is okay instead of done because this is a request
                data.result.should.equal('okay');
                clientPersona = data.persona;
                done();
            });
        });
        
        // list personas
        it ('persona: list all existing personas', function(done) {
            let args = {
                data: {
                    account: testAccountRef,
                    permit: testPermit                    
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/persona/list', args, function(data) {
                data.result.should.equal('okay');
                done();
            });
        });

        // inspect client persona 
        // note: can use qualifier or persona/ref direct ref
        //     first draft uses qualifier='client'
        it('persona: successfully inspects client persona', function (done) {
            let args = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    persona: 'client'
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/persona/inspect', args, function(data) {
                //console.log('raw persona data: ', JSON.stringify(data));
                data.result.should.equal('okay');
                // really need to check the actual returned data
                done();
            });
        });

        // then update client persona
        
        // test persona list
                
        // test persona toggles
        // test both activation and deactivation
        // starting with full name: 'name', base.testAccountFullName, 'full',

        it('persona: toggle client full name', function (done) {
            let args = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    persona: 'client',
                    attribute: 'name',
                    value: base.testAccountFullName,
                    qualifier: 'full',
                    state: true
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/persona/toggle', args,  function(data) {
                data.result.should.equal('done');
                // check more returned data
                done();
            });
        });

        // inspect client persona 
        // should show full name has been toggled
        it('persona: successfully inspects client persona', function (done) {
            let args = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    persona: 'client'
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/persona/inspect', args, function(data) {
                data.result.should.equal('okay');
                data.records[0].attribute.should.equal('name');
                data.records[0].value.should.equal(base.testAccountFullName);
                data.records[0].qualifier.should.equal('full');
                done();
            });
        });

        // inspect toggled client persona 
        // note: can use qualifier or persona/ref direct ref
        //     first draft uses qualifier='client'
        it('persona: successfully inspects toggled client persona', function (done) {
            let args = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    persona: 'client'
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/persona/inspect', args, function(data) {
                data.result.should.equal('okay');
                // check the actual returned data
                done();
            });
        });
        
        // test persona references? not currently implemented
        
        // test persona deletion, both provider and client
        it('persona: successfully deletes provider persona', function (done) {
            let args = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    name: 'provider'
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/persona/delete', args, function(data) {
                data.result.should.equal('done');
                done();
            });
        });
        it('persona: successfully deletes client persona', function (done) {
            let args = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    name: 'client'
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/persona/delete', args, function(data) {
                data.result.should.equal('done');
                done();
            });
        });
        
        // all testing done, now clean up environment by deleting account
        // first request account deletion
        it('persona: requests test account deletion', function(done) {
            let postDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', postDeleteArgs, function(data) {
                data.result.should.equal('okay');
                done();
            });
        });  // end request delete block
        // complete deletion with confirmation
        it('persona: confirms test account deletion', function(done) {
        let confirmDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', confirmDeleteArgs, function(data) {
                data.result.should.equal('done');
                done();
            });
        });  // end confirm delete block

    });  // describe valid and invalid inspection tests
}    // end of entire testPersonas test routine block

persona.test = testPersonas;
module.exports = persona;
