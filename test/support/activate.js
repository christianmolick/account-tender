// activate.js: activate account with invitation
//    also secure, maybe more ???
// require modules for service configuration and base testing
let assert = require('assert');
let config = require('../../app/config');
let base = require('./base');
//let account = require('../../app/account');

// define object for sharing test routine
var activate = {};

// internal storage of test account and permit references
var testAccountRef;
var testPermit;
var activationPermit;

// testCycle function is exported and executes all cycle tests
function testActivate() {
// account 44bccb91-4246-4756-88f0-15c19bdee903
    // invite bd9885fa-1d1a-449d-93c8-fd252c47d93f
    //
//base.seedAccount = '44bccb91-4246-4756-88f0-15c19bdee903';
//base.seedInvitation = 'bd9885fa-1d1a-449d-93c8-fd252c47d93f';
    // request account
    describe('activate account with invitation', function() {
        // !!! xxx
        it('activated account', function(done) {
            let activateAccountArgs = {
                data: {
                    code: base.seedCode
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/activate', activateAccountArgs, function(data) {
                console.log('activate returns: %s', JSON.stringify(data));
                assert.ok(data.result === 'okay');
                //assert.ok(data.reason === 'account-created');
                //testAccountRef = data.ref;
                // should return account = base.seedAccount
                // ... and also short name?
                // ref ???
                activationPermit = data.permit;
                done();
            });
        });
    });
    // this is combined here to easily use the activation permit

//base.seedAccount = '44bccb91-4246-4756-88f0-15c19bdee903';
//base.seedInvitation = 'bd9885fa-1d1a-449d-93c8-fd252c47d93f';
    // secure account
    describe('secure activates account with invitation permit', function() {
        // !!! xxx
        it('secured account', function(done) {
            let secureAccountArgs = {
                data: {
                    permit: activationPermit,
                    account: base.seedCode
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/secure', secureAccountArgs, function(data) {
                console.log('secure returns: %s', JSON.stringify(data));
                assert.ok(data.result === 'done');
                //assert.ok(data.reason === 'account-created');
                //testAccountRef = data.ref;
                // should return account = base.seedAccount
                // ... and also short name?
                done();
            });
        });
    });

}
activate.test = testActivate;
module.exports = activate;

