// update.js: Test account update functionality
// including invalid updates that should fail.

// require modules for service configuration, base testing,
// and arrange and complete to set up and clean up testing context
let assert = require('assert');
let config = require('../../app/config');
let base = require('./base');

// define object for sharing test routine
var update = {};

// testUpdate function is exported and executes all tests
function testUpdate() {
    describe('update: valid and invalid account inspection', function() {
        // first set up test context by making account and signing in
        
        // define variables to store account and sign in permit references
        var testAccountRef;
        var testPermit;
        // request account
        it('update: request account', function(done) {
            let requestAccountArgs = {
                data: {
                    credentials: [base.testChallenge, base.testResponse],
                    attributes: [
                        'name', base.testAccountName, 'short',
                        'name', base.testAccountFullName, 'full',
                        'contact', base.testEmail, 'primary email'
                    ]
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/request', requestAccountArgs, function(data) {                
                testAccountRef = data.ref;
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-created');
                done();
            });
        });  // it requests account block
        // request challenge for newly created test account
        it('update: request challenge', function(done) {
            // get challenge
            let getChallengeArgs = {
                data: {
                    name: base.testAccountName
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/challenge', getChallengeArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-read');
                done();
            });
        });  // it get challenge block
        // post response to challenge to complete account sign in
        it('update: post challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    account: testAccountRef,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/response', postResponseArgs, function(data) {
                testPermit = data.permit;
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'response-valid');
                done();
            });
        });  // it posts challenge response and receives permit block

        // now test inspection using the freshly made account and sign in permit
        // post changes to account data
        it('update: account update returns success', function(done) {
            let editArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    attributes: [
                        'name', base.alternativeFullName, 'full',
                        'phone', base.testPhone, 'primary phone'
                    ]
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/update', editArgs, function(data) {
                assert.ok(data.result === 'done');
                done();
            });
        });

        // inspect updated account to confirm updated records
        it('update: correctly recalls updated account data', function(done) {
            let inspectArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/inspect', inspectArgs, function(data) {
                assert.ok(data.result === 'okay');
                done();
            });
        });

        // all testing done, now clean up environment by deleting account
        // first request account deletion
        it('update: requests test account deletion', function(done) {
            let postDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', postDeleteArgs, function(data) {
                assert.ok(data.result === 'okay');
                done();
            });
        });  // end request delete block
        
        // complete deletion with confirmation
        it('update: confirms test account deletion', function(done) {
        let confirmDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/account/delete', confirmDeleteArgs, function(data) {
                assert.ok(data.result === 'done');
                done();
            });
        });  // end confirm delete block

    });  // describe valid and invalid inspection tests
}    // end of entire testInspection test routine block

update.test = testUpdate;
module.exports = update;
