// search.js: Test various ways of searching for accounts
// including invalid searches that should fail.

// !!! this is a first sketch which is not currently used
//     the most functionality tested in cycle tests

// require modules for service configuration and base values and routines
let config = require('../../app/config');
let base = require('./base');

// define object for sharing test routine
var search = {};

// internal storage of test account and permit references
var testAccountRef;
var testPermit;

// testSearch function is exported and executes all tests
function testSearch() {

    // account reference searches: specific qualified for provider
    describe('search for provider account using shared name and qualifier', function () {
        it('successfully returns correct account provider persona', function (done) {
            let args = {
                data: {
                    account: base.alternateAccount,
                    permit: base.alternatePermit,
                    name: base.testAccountName,
                    contact: base.alternativeEmail
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/inspect', args, function(data) {
                data.result.should.equal('okay');
                done();
            });
        });
    });

    // request alternate account data as client and provider
    describe('search for client account using shared name and qualifier', function () {
        it('successfully returns correct account client persona', function (done) {
            let args = {
                data: {
                    account: base.alternateAccount,
                    permit: base.alternatePermit,
                    name: base.testAccountName,
                    contact: base.alternativeEmail
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/account/inspect', args, function(data) {
                data.result.should.equal('okay');
                done();
            });
        });
    });

}
search.test = testSearch;
module.exports = search;
