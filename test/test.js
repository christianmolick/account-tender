// test.js: Execute tests for all account related functionality.
// This primarily loads modules and triggers tests
// from modules in the test/support folder.
// Note test modules currently proposed but not yet implemented:
// request, sign, search, reset

// local modules for configuration, basic 
let config = require('../app/config');  // service configuration required for access
let base = require('./support/base');  // base has essential values and get/post routines
let properties = require('./support/properties');  // fetch service properties
let activate = require('./support/activate');

//let cycle = require('./support/cycle');  // account cycle: create, sign in, inspect, delete
//let reference = require('./support/reference');  // lookups which may be ambiguous
//let inspect = require('./support/inspect');  // ...
//let update = require('./support/update');  // ...
//let request = require('./support/request');  // ...
//let persona = require('./support/persona');  // ...

// execute initial tests that require only basic values and routines
properties.test();
activate.test();
//cycle.test();

// execute tests that require arrangement and completion
// !!! TESTS SUSPENDED TO SIMPLIFY FAILURE CASCADE
//reference.test();
//inspect.test();
//update.test();  // ??? no longer used?
//request.test();
// !!! 2021-12-21 persona interfaces being refactored
//persona.test();

